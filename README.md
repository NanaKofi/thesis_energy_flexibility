# Jupyter notebooks and code (supplementary materials) for the thesis: ["Direct and indirect energy flexibility interactions at the building and community scale: From system to the human-interfaced system"](hal.fr)
In this repository, we provide all the code and notebooks, as supplementry materials for the research work carried out during the thesis.
This repositry is intended to be an interactive appendix to the thesis, and additionally a means to facilitate open and reproducable energygy research.

We encourage users to modify and adapt the code to different use cases and applications.

## AUTHOR:
- **Name** : Nana Kofi Baabu TWUM-DUAH
- **Contact**: nana-kofi-baabu.twum-duah@grenoble-inp.fr

## SUPERVISORS
* **Frédéric WURTZ** - Research Director, CNRS, France
* **Stéphane PLOIX** - Professor, Grenoble Institute of Technology, France 
* **Manar AMAYRI** - Assistant professor, Concordia University, Canada


**Keywords**: Energy Flexibility, Self-Consumption, Multi-Agent Systems, Model Predictive Control, Human-in-the-Control-System, Human-in-the-Loop, Electric Vehicles, Local Energy Communities.


## Abstract
### English Version

In the context of the energy transition and the need for decarbonization through the use of renewable energies (i.e., the energy transition), the building industry assumes a pivotal role due to its potential to generate local renewable energy and its substantial energy consumption. Buildings (and by extension the energy grid) have two key components which are central to this thesis, the energy subsystem (i.e., the technical component and in essence the direct flexibility) and the human occupants (i.e., the social, indirect flexibility, component, which is key to determining the energy performance of any building). In most cases, the emphasis is placed on controllable direct flexibility. However, there is a need for the two components to work together to produce flexibility services both for the grid and the building (in the context of self-consumption). 
 
This thesis explores the potential synergy that exists between the human occupant and a building’s energy subsystem in the context of energy flexibility using the Predis-MHI platform (a living lab within the GreEN-ER building) and its users as an experimental setup. This setup was constituted by a 22kWp solar PV station, the demand of the building, EV charging stations, and a 50kWh battery. For this research, we identified EV charging (in particular the plugging in and unplugging) as an indirect flexibility resource. 
 
Our investigation follows a three-part methodology to address the challenges associated with human-interfaced buildings, first, we propose a Mixed Integer Linear Programming optimization approach to assess the available potential of indirect flexibility and subsequently to dimension direct flexibility. The second aspect entailed the control of direct flexibilities, for this, we proposed and implemented a Machine Learning based Model Predictive Controller. Lastly, to gain insight into the interplay between the direct and indirect flexibilities, we propose a co-simulation approach based on a Mixed Integer Linear Programming optimization and a Multi-Agent System simulation of the stochastic behavior of EV users concerning charging their vehicles. This co-simulation is envisioned to allow for the testing of different scenarios (and rules, especially in the context of a local energy community). 
 
Our findings show that indirect flexibilities are not only essential to the energy transition, but they are quantifiable, with a discernible impact on the overall performance of the building system. In the case of our test bed, the estimated potential of the indirect flexibility in terms of self-consumption is approximately 8\% (1,700.00 kWh) annually. The Machine Learning-based Model Predictive Controller combined with the reactive controller of the 50kWh battery also showed promise despite a decreased performance particularly attributed to the low quality of EV demand forecasts. Lastly, the co-simulation indicates that if mobilized, indirect flexibility can be utilized as a primary energy flexibility resource contrary to current practice where direct flexibility is the preferred resource for providing energy flexibility. 

### Version Française
Dans le contexte de la transition énergétique et du besoin de décarbonisation par l'utilisation d'énergies renouvelables ("la transition énergétique"), l'industrie du bâtiment joue un rôle central en raison de son potentiel de production d'énergie renouvelable locale et de sa consommation d'énergie significative. Les bâtiments (et par extension le réseau énergétique) ont deux composantes clés qui sont au cœur de cette thèse, le sous-système énergétique ("la composante technique" et par essence la flexibilité directe) et les occupants humains ("la composante sociale", la flexibilité indirecte, qui est essentielle pour déterminer la performance énergétique de n'importe quel bâtiment). Dans la plupart des cas, l'accent est mis sur la flexibilité directe contrôlable. Cependant, il est nécessaire que les deux composantes travaillent ensemble pour produire des services de flexibilité à la fois pour le réseau et pour le bâtiment (dans le contexte de l'autoconsommation). 
 
Cette thèse explore la synergie potentielle qui existe entre l'occupant humain et le sous-système énergétique d'un bâtiment dans le contexte de la flexibilité énergétique en utilisant la plateforme Predis-MHI (un laboratoire vivant au sein du bâtiment GreEN-ER) et ses utilisateurs comme dispositif expérimental. Cette configuration était constituée d'une station solaire photovoltaïque de 22 kWc, de la demande du bâtiment, de stations de recharge pour VE et d'une batterie de 50 kWh. Pour cette recherche, nous avons identifié la recharge des VE (en particulier le branchement et le débranchement) comme une ressource de flexibilité indirecte. 

Notre étude suit une méthodologie en trois parties pour relever les défis associés aux bâtiments à interface humaine. Nous proposons tout d'abord une approche d'optimisation par programmation linéaire en nombres entiers pour évaluer le potentiel disponible de flexibilité indirecte et, par la suite, pour dimensionner la flexibilité directe. Le deuxième aspect concerne le contrôle des flexibilités directes, pour lequel nous avons proposé et mis en œuvre un contrôleur prédictif de modèle basé sur l'apprentissage automatique. Enfin, pour mieux comprendre l'interaction entre les flexibilités directes et indirectes, nous proposons une approche de co-simulation basée sur une optimisation de programmation linéaire en nombres entiers et une simulation de système multi-agents du comportement stochastique des utilisateurs de VE en ce qui concerne la recharge de leurs véhicules. Cette co-simulation est conçue pour permettre de tester différents scénarios (et règles, en particulier dans le contexte d'une communauté énergétique locale).

Nos résultats montrent que les flexibilités indirectes sont non seulement essentielles à la transition énergétique, mais qu'elles sont aussi quantifiables, avec un impact perceptible sur la performance globale du système de construction. Dans le cas de notre banc d'essai, le potentiel estimé de la flexibilité indirecte en termes d'autoconsommation est d'environ 8\% (1 700 kWh) par an. Le contrôleur prédictif de modèle basé sur l'apprentissage automatique combiné au contrôleur réactif de la batterie de 50 kWh s'est également avéré prometteur malgré une baisse de performance attribuée en particulier à la faible qualité des prévisions de la demande des VE. Enfin, la co-simulation indique que si elle est mobilisée, la flexibilité indirecte peut être utilisée comme une ressource de flexibilité énergétique primaire, contrairement à la pratique actuelle où la flexibilité directe est la ressource préférée pour fournir une flexibilité énergétique. 


## Available Jupyter notebooks
### Chapter Two
- Analysis of Predis-MHI (the experimental setup) data: [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_2_Predis_MHI_Data_Analysis.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_file/Chapter_2_Predis_MHI_Data_Analysis.html)

## Chapter Three
- Proposed Framework for assessing indirect flexibility potential [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_3_indirect_flexibility_assessment.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_3_indirect_flexibility_assessment.html)
- Proposed sizing method which takes into account indirect flexibility: [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_3_Battery_sizing.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_3_Battery_sizing.html)

## Chapter Four
- Comparison of rule-based and optimization-control for stationary battery storage [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_4_Comparison_of_control.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_4_Comparison_of_control.html)
- Imlpementation and comparison of Machine Learning based forecasting models: [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_4_Comparison_of_Forecasting_Models.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_4_Comparison_of_Forecasting_Models.html)
- Implementation of Machine Learning-based Model Predictiv Control: [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_4_MPC_implementation.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_4_MPC_implementation.html)

## Chapter Four
- Implementation of co-simultion tool for EV energy community: [Jupyter Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/notebooks/Chapter_5_Co-simulation.ipynb) | [HTML File](https://gricad-gitlab.univ-grenoble-alpes.fr/NanaKofi/thesis_energy_flexibility/-/blob/main/html_files/Chapter_5_Co-simulation.html)

## Contact
https://www.jonathancoignard.com/



## Getting started
all notebooks are in the directory ````\notebooks````, however since git does not render html plots, we have provided html versions of each version in the folder  ```html_files```.

To run the notebooks, you will need to install all the dependencies. There are 2 ways to do this,
the first and easier is to uncomment the cell block (there is a note befrore it) that has the code ``pip install -e .``, it typically looks like this:
![img.png](images/img.png).

Alternatively, you can open a comand line interface from the root dierctory and simly run the command ``pip install -e .``

<span style="color:red">**NOTE**: You only need to install the dependacies once. if it was properly isntalled all notebooks should run fine</span>.


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## Project status
The Notebooks and code are not the final version, however the reuslts depicted re still relevant.