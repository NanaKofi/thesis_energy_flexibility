import holidays
import pandas as pd
import pickle as pk


def calendar(df,index = "datetime") :
    df = df.reset_index()
#     print(df)
    df['Year'] = df[index].dt.year
    df['Month_name'] = df[index].dt.month_name()
    df['Weekday'] = df[index].dt.day_name()
    df['Day_number'] = df[index].dt.day
    df['Hour'] = df[index].dt.hour
    df["week"] =  df[index].dt.isocalendar().week
    df["date"] =  df[index].dt.date

    df = df.set_index(index)

    df['is_holiday'] = 0
    df['is_weekend'] = 0

    ### Public Holidays
    fr = holidays.FR()

    # df.loc[df["date"] in  fr, 'calendar'] = 0
    for d, ind in zip(df["date"] , df.index) :

        if d in fr :
            df.loc[ind,"is_holiday"] = 1


    ### weekends
    df.loc[(df['Weekday'] == 'Saturday'), 'is_weekend'] = 1
    df.loc[(df['Weekday'] == 'Sunday'), 'is_weekend'] = 1

    ####COVID Lockdown:
    df.loc[(df['Month_name'] == "March") & (df["Day_number"] >= 16) & (df["Year"] == 2020), 'is_holiday'] = 1
    df.loc[(df['Month_name'] == "April") & (df["Year"] == 2020), 'is_holiday'] = 1
    df.loc[(df['Month_name'] == "May") & (df["Day_number"] <= 11) & (df["Year"] == 2020), 'is_holiday'] = 1
    df.loc[(df['Month_name'] == 'June') & (df['Year'] == 2020), 'is_holiday'] = 1
    df.loc[(df['Month_name'] == 'September') & (df['Year'] == 2020), 'is_holiday'] = 1



    df.loc[(df['week'] >=51 ) , 'is_holiday'] = 1
    df.loc[(df['Month_name'] == "July") & (df["Day_number"] == 31 ), 'is_holiday'] = 1
    df.loc[(df['Month_name'] == "August") & (df["Day_number"] <= 15), 'is_holiday'] = 1

    try:
        df.drop(columns=['Year',"date","week", 'Month_name', 'Weekday', 'Day_number', 'Hour', 'TMP'], inplace=True)

    except KeyError:
        df.drop(columns=['Year',"date", "week", 'Month_name', 'Weekday', 'Day_number', 'Hour'], inplace=True)

    return df


def reconstruct(results, prefix, horizon):
    new = []
    for i in range(len(results[f"{prefix}_0"])):

        for j in range(horizon):
            new.append(results[f"{prefix}_{j}"][i])

    return new


def reshape_(df=None, horizon=24, ref="PV_production"):
    temp = {key: [] for key in [col + f"_{i}" for col in df.columns for i in range(horizon)]}
    new_ind = []
    for col in df.columns:
        for index, frame in df[col].groupby([(df.index.month), (df.index.day), (df.index.year), df.index.hour]):
            #             print (frame.values[0])
            temp[col + f"_{frame.index[0].hour}"].append(frame.values[0])

            if col == ref and frame.index[0].hour == 0:
                #                 frame.index[0].date
                new_ind.append(frame.index[0])
            else:
                pass
    #                 print (frame.index)

    #             break

    #     print (new_ind , temp)
    return (pd.DataFrame(temp, index=new_ind).sort_index(axis=0, ascending=True))