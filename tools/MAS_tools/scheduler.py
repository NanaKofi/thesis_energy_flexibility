from mesa.time import RandomActivation, SimultaneousActivation



class SimultaneousChargers (SimultaneousActivation):
    def __init__(self, model ):
        super().__init__(model)

    def step(self,to_charge,  datetime =None   ):

        if datetime is not None:
            agent_keys = list(self._agents.keys())
            for agent_key in agent_keys:
                self._agents[agent_key].step(to_charge, datetime.hour, closure=19, opening=7)
            for agent_key in agent_keys:
                self._agents[agent_key].advance()
            self.steps += 1
            self.time += 1
        else:
            super().step()


class RandomUser (RandomActivation):
    def __init__(self, model):
        super().__init__(model)

    def add(self, agent: list) -> None:
        """Add an Agent object to the schedule.

        Args:
            agent: An Agent to be added to the schedule. NOTE: The agent must
            have a step() method.

        """
        ev , human = agent
        u_id = [ev.unique_id , human.unique_id]
        if u_id in self.agents:
            raise Exception(
                "EV and Human Agent with unique ids {0}{1} already added to scheduler".format(
                    repr(ev.unique_id) ,repr( human.unique_id)
                )
            )

        # print (type(self._agents))
        self._agents[human.unique_id] = agent


    def step(self,avail_chargers,charger_agents ,  datetime =None, nudges = None, sim_type = "advanced"):

        if datetime is not None:

            for ev , human in self.agent_buffer(shuffled=True):
                try:
                    # print(f"here :  {human.unique_id - 1}")
                    nudge = nudges[human.unique_id - 1][datetime]
                except:
                    # print(nudges[human.unique_id - 1])
                    nudge = nudges[human.unique_id - 1][datetime.hour]
                human.nudge = nudge
                human.step(ev = ev,  datetime = datetime,sim_type = sim_type)
                # print ("here")
                ev.step(charger_agents = charger_agents,chargers = avail_chargers , human = human)

            self.steps += 1
            self.time += 1
        else:
            super().step()
