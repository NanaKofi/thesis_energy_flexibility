from mesa import Agent
import random
import numpy as np
import math


class EV_Owner_Agent(Agent):

    def __init__(self, unique_id,agent_name , time_to_work, time_to_home,
                 daily_dist,ev_id, model,type = 1,motivation =1):
        # print ("UNIQUE:  ", unique_id)
        super().__init__(unique_id, model)
        self.agent_name = agent_name
        self.ev_id = ev_id #todo Update with ID of EV owned by agent #done

        self.motivation = motivation
        self.time_to_home = time_to_home
        self.time_to_work = time_to_work

        self.daily_dist = daily_dist
        self.work_hours = 0
        self.available = 0
        # self.schedule = self.gen_schedule()
        self.at_work = False
        self.at_home = True
        self.at_other = False
        self.sat_space = list([1]) * 12
        self.type = type
        self.schedule = self.gen_schedule()
        self.nudge = 0
        self.projected_demand = 0

    def get_demand(self):
        demand = self.daily_dist

    def gen_schedule(self, start=8, end=20, horizon=24):

        data = [0] * horizon  # Initialize the list with zeros
        while sum(data) <= 2:
            for i in range(start, end):
                if i >= 8 and i <= 10 and sum(data) < 1:
                    probability = 0.85  # Higher likelihood of 1 within positions 8-10
                elif i == 12 or i == 17:
                    probability = 0.72  # Higher likelihood of 1 at positions 12 and 17
                else:
                    probability = 0.4  # Normal likelihood of 1 for other positions

                data[i] = random.choices([0, 1], weights=[1 - probability, probability])[0]

        return data




    def step(self,datetime, ev ,sim_type):

        self.move(time = datetime.hour,ev=ev,sim_type=sim_type)
        ev.demand = ev.batt_cap - ev.avail_bat
        # self.charge(chargers = chargers,charger_agents = charger_agents,ev = ev)






        # print (f"TEST : {self.available}")


    def move(self, time,ev,sim_type):

        def sigmoid(hour, k=0.1, x0=10):
            return 1 / (1 + math.exp(-k * (hour - x0)))


        closure = 19
        ###MAKE sure SOC doesnt go below Zero, charging happens outside the building premisis
        if self.at_other and ev.soc <=0:
            ev.soc = random.randint(15,50)
            ev.avail_range = ev.avail_range = ev.soc / 100 * ev.max_range
        self.available = self.schedule[time]
        # print(f"TEST 1 : {self.available} , available = {self.schedule[time]}, time = {time}")
        # self.available = self.get_availability(planned = self.schedule[time] , time= time)
        # print(f"TEST 2 : {self.available}")
        # print("available range at start : {}".format(ev.avail_range))


        #############################################################
        # Simple is oonly for testing purposes
        if sim_type== None or  sim_type == "simple":

            if time > 7 and time<20 and self.at_work == False:
                self.available = 1
                decision = "to_work"
            elif time > 7 and time < 20 and self.at_work == True:
                self.available = 1

                decision = "stay"

            elif time == 20 and self.at_work == True:
                decision = "to_home"

            elif time <= 7 or time > 20 :
                decision = "Stay"





        elif sim_type == "advanced":

            if self.available == 1 and  self.at_work ==1:
                decision = "stay"


            elif self.available == 1 and  self.at_work ==0:
                decision = "stay"



            elif self.available == 0:




                if  time <= 7 and self.at_home == False:
                    decision = "to_home"

                elif time > 20 and self.at_home == False:
                    decision = "to_home"

                elif time > 7 and time< 20:
                    if self.at_home and time <= 12:
                        options = ["to_other","to_work", "stay"]
                        p = sigmoid(hour = time, k=0.4, x0=7)
                        p = [(1-p)/3,p,(2 * (1-p))/3]
                        # p = [0.05, 0.85, 0.1]

                    elif self.at_home and time > 12:
                        options = ["to_other", "to_work", "stay"]
                        p = sigmoid(hour=time, k=0.3, x0=6)
                        p = [(1 - p) / 3, (2 * (1 - p)) / 3, p]

                    elif self.at_work :
                        options = ["to_other", "to_home", "stay"]
                        p = [0.1,0.1,0.8]
                        if ev.soc < 5:
                            p = [0,0,1]

                    elif self.at_other:
                        if ev.avail_range < 5:
                            ev.soc = random.randint(15,50)
                            ev.avail_range = ev.soc / 100 * ev.max_range
                        options = ["to_work", "to_home", "stay"]
                        p = sigmoid(hour = time, k=0.3, x0=9)

                        if time <14:
                            p = [(3 * (1-p))/4 , (1-p)/4 , p]

                        else:
                            p = [ (1 - p) / 4,(3 * (1 - p)) / 4, p]


                    decision = np.random.choice(options, p=p, size=1)[0]

                else:
                    decision = "stay"

        if decision == "Stay" or decision == "stay":
            if self.at_work == False:
                self.available = 0

            usage = 0
            dist = 0
            # if self.at_work == True:
            #     self.get_availability(planned = [self.schedule[time]] )




        elif decision == "to_home":
            self.available = 0
            ev.location = "home"
            # self.available = 0
            ev.charger_id = 0
            self.at_home = True
            self.at_work = False
            self.at_other = False
            dist = self.daily_dist * 0.5


        elif decision == "to_other":
            self.available = 0

            if ev.soc < 5 :
                ev.soc = random.randint(10, 50)
                ev.avail_range = ev.soc / 100 * ev.max_range

            try:
                dist = random.random() * random.randint(2, int(0.5 * ev.avail_range))
            except:
                # print ("AVAILABLE RANGE : " , ev.avail_range,"   ", ev.soc)
                # print("Location : ",ev.location, self.at_work,self.at_home, self.at_other)
                dist = random.random() * random.randint(2, int(0.5 * ev.avail_range))
            # usage = dist / ev.km_p_kwh  # kwh
            ev.location = "other"
            self.at_home = False
            self.at_work = False
            self.at_other = True

        elif decision == "to_work" and self.available == 0 :
            # self.available = 1
            # self.available = self.get_availability(planned = self.schedule[time] , time= time)


            # self.get_availability(planned=[self.schedule[time]])

            ev.location = "work"
            self.at_home = False
            self.at_work = True
            self.at_other = False
            dist = self.daily_dist * 0.5
            # usage = dist / ev.km_p_kwh  # kwh
            ev.test = ev.batt_cap - ((ev.soc * ev.batt_cap * 0.01) - ((0.5 * self.daily_dist)/ ev.km_p_kwh))

        ev.avail_range -= dist

        usage = dist / ev.km_p_kwh
        ev.usage = usage
        ev.distance = dist
        cap = ev.batt_cap * (ev.soc * 0.01)
        if cap - usage < 0:
            ev.soc = random.randint(10, 30)

        else:
            ev.soc =  ((cap - usage) / ev.batt_cap) * 100
        # ev.soc = (ev.avail_range / ev.max_range) * 100
        # ev.avail_range = ev.soc / 100 * ev.max_range
        ev.avail_bat = (ev.soc / 100) * ev.batt_cap
        ev.demand = ev.batt_cap - ev.avail_bat
