from pyomo.opt import SolverFactory
from pyomo.environ import *
import pandas as pd, numpy as np


def reshape_df(rows=24, columns=4, df=None):

    data = df[df.columns[0]].tolist()
    new = pd.DataFrame()

    for i in range(columns):
        new[i + 1] = data[i * rows: (i + 1) * rows]
    return new

class Optimizer():

    def __init__(self,capacity: float = 50, max_dis: float = -40, max_chg: float = 50,sd=1e-6,
                     ch_eff: float = 0.98,min_soc: float = 10, dch_eff: float = 0.95,max_soc: float = 10):
        self.capacity = capacity
        self.max_dis = max_dis
        self.max_chg = max_chg
        self.ch_eff = ch_eff
        self.dch_eff = dch_eff
        self.min_soc = min_soc
        self.max_soc = max_soc
        self.sd = sd




    def _get_pyomo_timeseries(self, result, demand, production, dates,  model, keys=None, sch = True):
        """Custom function to retrive optimization timeseries from `base.py`"""

        if keys == None:
            keys = ["grid_import", "grid_export", "battery_in", "battery_out", "battery_energy",
                    "charger_state", 'chargers', "charge_sch", "discharge_sch"]

        if sch:
            owners = model.owners.data()
            chargers = model.chs.data()
            ts = model.ts.data()

            for key in keys:
                df = pd.DataFrame(index=["_"], data=getattr(model, key).get_values())
                df = df.transpose()
                if "charger" in key:
                    key = [f"{key}_{i + 1}" for i in range(4)]
                    df = reshape_df(columns=len(key), rows=int(len(df) / len(key)), df=df)
                    df.columns = key
                    for k in key:
                        result[k] = df[k].tolist()

                elif "sch" in key:

                    for owner in owners:

                        for charger in chargers:

                            result[f"{key}_{owner}_{charger}"] = []
                            #                     result[f"schedule_list{owner+1}"] =
                            for t in ts:

                                try:
                                    result[f"{key}_{owner}_{charger}"].extend(
                                        df.loc[owner, charger, t, :].copy().values[-1])
                                except ValueError:
                                    result[f"{key}_{owner}_{charger}"].extend(
                                        df.loc[owner, charger, t].copy())
                else:
                    df.columns = [key]
                    #
                    result[key] = df[key].tolist()

        else:
            for key in keys:
                df = pd.DataFrame(index=["_"], data=getattr(model, key).get_values())
                df = df.transpose()
                df.columns = [key]
                result[key] = df[key].tolist()


        result = pd.DataFrame(result)
        result["battery_power"] = - (result["battery_in"] + result["battery_out"])
        result["production"] = production.values
        result["load"] = demand.values
        result["dates"] = dates

        return result


    def get_nudges(self, chargers:list, owners, opt_1):
        nudges = []
        for owner in owners:
            opt_1[f"Schedule_{owner}"] = [0] * 24
            for charger in chargers:
                opt_1[f"Schedule_{owner}"] += (opt_1[f"charge_sch_{owner - 1}_{charger - 1}"] - opt_1[
                    f"discharge_sch_{owner - 1}_{charger - 1}"]) * charger

            nudges.append(opt_1[f"Schedule_{owner}"].tolist())
            # opt_1 = opt_1.drop(
            #     columns=[f"charge_sch_{owner - 1}_{charger - 1}", f"discharge_sch_{owner - 1}_{charger - 1}"])

        return nudges


    def opt_individual(self, max_soc: float = 50,  ev_avail=[0], prod: list = None, load: list = None,
                          init_cap: float = 50, demand=[0], datess: list = None,  timestep=60,
                           solver='gurobi', verbose=False, solver_path=None,owners=None):
        # Capacity: capacity of battery storage in KWh (could be in Wh but all parameters should be in W)
        # max_dis :  maximum discharge powwer value of the battery (should be negative)
        # max_chg : maximum charging power valu of the battery
        # ch_eff : Battery charghing efficiency (between 1 and 0)
        # dch_eff : Battery discharghing efficiency (between 1 and 0)
        # max_soc : Maximum allowed state of charge (0 <= max_soc <= capacity)
        # min_soc : Minimum allowed state of charge (0 <= min_soc <= capacity)
        # prod : list containing the PV production values (PV production profile)
        # load : list containing the demand profile
        # period : number of hours for which the evaluation is being done (currently only considers 1 hour time step)
        # datess : currently unused
        # ch_1 : Energy consumed by charger 1 during the evaluation period
        # ch_2 : Energy consumed by charger 2 during the evaluation period
        # co2_grid : co2 emmisions of the grid in gCO2_eq/kwh
        # returns a dataframe with all the calculated porameters

        m = ConcreteModel()
        timestep = timestep / 60
        max_charge = 7

        no_ch = 4
        m.ts = Set(initialize=list(range(0, len(prod))), ordered=True)
        m.chs = Set(initialize=list(range(0, no_ch)), ordered=True)
        m.owners = Set(initialize=list(range(0, len(ev_avail))), ordered=True)

        m.c = Var(m.owners, m.chs, domain=NonNegativeIntegers)
        m.d = Var(m.owners, m.chs, domain=NonNegativeIntegers)

        last = m.ts.last()

        m.grid_import = Var(m.ts, domain=NonNegativeReals)
        m.t_load = Var(m.ts, domain=NonNegativeReals)
        m.battery_in = Var(m.ts, domain=NegativeReals)
        m.battery_out = Var(m.ts, domain=NonNegativeReals)
        m.battery_energy = Var(m.ts, domain=NonNegativeReals, bounds=(self.min_soc, self.max_soc))
        m.battery_state = Var(m.ts, domain=Binary, bounds=(0, 1))
        m.grid_export = Var(m.ts, domain=NegativeReals)

        m.charge_sch = Var(m.owners, m.chs, m.ts, domain=Binary, bounds=(0, 1))
        m.discharge_sch = Var(m.owners, m.chs, m.ts, domain=Binary, bounds=(0, 1))

        m.charge_st = Var(m.owners, m.chs, m.ts, domain=Binary, bounds=(0, 1))

        m.chargers = Var(m.chs, m.ts, domain=NonNegativeReals, bounds=(0, 7))
        m.charger_state = Var(m.chs, m.ts, domain=Binary, bounds=(0, 1), initialize=0)

        m.demand_ind = Var(m.owners, m.chs, domain=NonNegativeReals)

        m.end_cap = Var(domain=NonNegativeReals, bounds=(self.min_soc, self.max_soc))

        #################################################### Param
        m.self_consumption = Var(domain=NonNegativeReals)  # ,bounds=(0,1))
        m.obj = Var(domain=NonNegativeReals)
        m.demand = Var(domain=NonNegativeReals)
        m.total_co2 = Var(domain=NonNegativeReals)

        #################################################### Rules
        # --------------------------------------------------------
        # ---------------------Time----------------------------
        # --------------------------------------------------------

        # ensures EV charging only happens during buiding operating hours

        def r_ev_charging_time(m, t):
            if datess[t].hour <= 8 or datess[t].hour >= 20:
                return (sum(m.chargers[c, t] for c in m.chs) == 0)
            else:
                return Constraint.Skip

        # ensure that EV plugs in before being unplugged
        def place_b4_displace(m, o):

            if sum(ev_avail[o]) <= 1:
                return (sum(sum(m.discharge_sch[o, c, t] * datess[t].hour for c in m.chs) for t in m.ts) -
                        sum(sum(m.charge_sch[o, c, t] * datess[t].hour for c in m.chs) for t in m.ts) == 0)

            else:
                return (sum(sum(m.discharge_sch[o, c, t] * datess[t].hour for c in m.chs) for t in m.ts) -
                        sum(sum(m.charge_sch[o, c, t] * datess[t].hour for c in m.chs) for t in m.ts) >= 1)

                # --------------------------------------------------------

        # ---------------------EV Charging----------------------
        # --------------------------------------------------------

        # ensures no activity if users is busy , else optimizer can decide to place vehicle
        def r_place_EV(m, o, t):
            if ev_avail[o][t] == 0:

                return (sum(m.charge_sch[o, c, t] for c in m.chs) == 0)

            else:

                if datess[t].hour <= 8 or datess[t].hour >= 20:
                    return (sum(m.charge_sch[o, c, t] for c in m.chs) == 0)
                else:
                    return (sum(m.charge_sch[o, c, t] for c in m.chs) <= 1)

        # ensures that at each time, the maximum number of active chargers is the number of chargers
        def r_sim_charge(m, t):
            return (sum(m.charger_state[c, t] for c in m.chs) <= no_ch)

        #     def r_ind_charge(m, t):
        #         return (sum(m.charger_state[c, t] for c in m.chs) <= no_ch)

        # Ensures EV charges at most once and at one charger
        def r_EV_charge_once(m, o):
            return (sum(sum(m.charge_sch[o, c, t] for t in m.ts) for c in m.chs) <= 1)

        # ensures an EV is only at one charging station
        def r_simul(m, c, t):
            return (sum(m.charge_sch[o, c, t] for o in m.owners) <= 1)

        # --------------------------------------------------------
        # ---------------------EV Discharging---------------------
        # --------------------------------------------------------
        # ensures no activity if users is busy , else optimizer can decide to displace vehicle
        def r_displace_EV(m, o, t):
            if ev_avail[o][t] == 0:

                return (sum(m.discharge_sch[o, c, t] for c in m.chs) == 0)

            else:
                if datess[t].hour <= 8 or datess[t].hour >= 20:
                    return (sum(m.discharge_sch[o, c, t] for c in m.chs) == 0)
                else:
                    return (sum(m.discharge_sch[o, c, t] for c in m.chs) <= 1)

        # Ensures EV is displaced from the charger it was placed on
        def r_EV_disccharge_once(m, o, c):
            return (sum(m.charge_sch[o, c, t] for t in m.ts) == sum(m.discharge_sch[o, c, t] for t in m.ts))


        def r_charger_status_ind(m, o, c, t):
            if datess[t].hour <= 8 or datess[t].hour >= 20:
                return (m.charge_st[o, c, t] == 0)
            elif sum(ev_avail[i][t] for i in m.owners) == 0:
                return (m.charge_st[o, c, t] == m.charge_st[o, c, t - 1])
            else:
                #             return Constraint.Skip
                return (m.charge_st[o, c, t] == m.charge_st[o, c, t - 1] + (
                            ev_avail[o][t] * (m.charge_sch[o, c, t] - m.discharge_sch[o, c, t])))

        def r_charger_status(m, c, t):
            return (m.charger_state[c, t] == sum(m.charge_st[o, c, t] for o in m.owners))

        # ensures chargin only occurs when the charger is occupied
        def r_charging(m, c, t):

            return (m.chargers[c, t] <= max_charge * m.charger_state[c, t])

        # m.demand is the remaining demand
        def r_demand(m):
            return (sum(demand[o] for o in m.owners) - sum(sum(m.chargers[c, t] for c in m.chs) for t in m.ts) == m.demand)

        # defines how much of the demand should be met
        def r_charger_total(m):
            return (sum(sum(m.chargers[c, t] for t in m.ts) for c in m.chs) <= (sum(demand)))

        # --------------------------------------------------------
        # ---------------------Battery----------------------------
        # --------------------------------------------------------

        def r_battery_max_powerin(m, t):
            return (m.battery_in[t] >= -self.max_chg * m.battery_state[t])

        def r_battery_max_powerout(m, t):
            return (m.battery_out[t] <= self.max_dis * (m.battery_state[t] - 1))

        def r_battery_energy(m, t):
            if t == 0:
                return m.battery_energy[t] == init_cap
            else:
                return (m.battery_energy[t - 1] * (1 - self.sd) - m.battery_energy[t] ==
                        m.battery_in[t - 1] * timestep * self.ch_eff + m.battery_out[t - 1] * timestep / self.dch_eff)

        def r_battery_min_energy(m, t):
            return (m.battery_energy[t] >= self.min_soc)

        def r_battery_max_energy(m, t):
            return (m.battery_energy[t] <= max_soc)

        def r_end_cap(m):
            return (m.battery_energy[last] * (1 - self.sd) - m.battery_in[last] * timestep * self.ch_eff - m.battery_out[
                last] * timestep / self.dch_eff
                    == m.end_cap)

        def r_battery_max_end_energy(m):
            return (m.end_cap <= max_soc)

        def r_battery_min_end_energy(m):
            return (m.end_cap >= self.min_soc * 1.001)

        # --------------------------------------------------------
        # ------------------Grid --------------------------------
        # --------------------------------------------------------
        def r_energy_balance(m, t):
            return (prod[t] - (load[t] + sum(m.chargers[c, t] for c in m.chs)) + m.grid_export[t]
                    + m.grid_import[t] + m.battery_in[t] + m.battery_out[t] == 0)

        def r_grid_export(m, t):
            return (-m.grid_export[t] <= prod[t])

        def r_grid_import(m, t):
            return (m.grid_import[t] <= load[t] + sum(m.chargers[c, t] for c in m.chs))

        def r_load(m, t):

            return (load[t] + sum(m.chargers[c, t] for c in m.chs) == m.t_load[t])

        def r_self_consumption(m):
            return (sum(m.t_load[t] - m.grid_import[t] for t in m.ts) / sum(prod) == m.self_consumption)

        def r_objective(m):
            return (m.demand + 0.00 * sum(sum(m.charger_state[c, t] for t in m.ts) for c in m.chs) +
                    sum(m.grid_import[t] for t in m.ts) - sum(m.grid_export[t] for t in m.ts) == m.obj)


        def objective_function(m):
            return m.obj

        # --------------------------------------------------------
        # ------------------Add constraints --------------------------------
        # --------------------------------------------------------

        m.r1 = Constraint(m.ts, rule=r_ev_charging_time)
        m.r2 = Constraint(m.owners, rule=place_b4_displace)
        m.r3 = Constraint(m.owners, m.ts, rule=r_place_EV)

        m.r4 = Constraint(m.ts, rule=r_sim_charge)
        m.r5 = Constraint(m.owners, rule=r_EV_charge_once)
        m.r6 = Constraint(m.chs, m.ts, rule=r_simul)
        m.r7 = Constraint(m.owners, m.ts, rule=r_displace_EV)

        m.r8 = Constraint(m.owners, m.chs, rule=r_EV_disccharge_once)
        m.r9 = Constraint(m.chs, m.ts, rule=r_charger_status)
        m.r9a = Constraint(m.owners, m.chs, m.ts, rule=r_charger_status_ind)
        m.r10 = Constraint(m.chs, m.ts, rule=r_charging)
        m.r11 = Constraint(rule=r_demand)

        m.r12 = Constraint(rule=r_charger_total)
        m.r13 = Constraint(m.ts, rule=r_battery_max_powerin)
        m.r14 = Constraint(m.ts, rule=r_battery_max_powerout)
        m.r15 = Constraint(m.ts, rule=r_battery_energy)

        m.r16 = Constraint(m.ts, rule=r_battery_max_energy)
        m.r17 = Constraint(m.ts, rule=r_battery_min_energy)
        m.r18 = Constraint(rule=r_end_cap)
        m.r19 = Constraint(rule=r_battery_min_end_energy)

        m.r20 = Constraint(rule=r_battery_max_end_energy)
        m.r21 = Constraint(m.ts, rule=r_energy_balance)
        m.r22 = Constraint(m.ts, rule=r_grid_export)
        m.r23 = Constraint(m.ts, rule=r_grid_import)
        m.r24 = Constraint(m.ts, rule=r_load)
        #m.r25 = Constraint(rule=r_self_consumption)

        m.r26 = Constraint(rule=r_objective)


        m.objective = Objective(rule=objective_function, sense=minimize)

        m.write("new.lp")

        with SolverFactory(solver, executable=solver_path) as opt:
            result = opt.solve(m, tee=False)
            if verbose:
                print(result)

        #     log_infeasible_constraints(m, log_expression=True, log_variables=True)
        #     logging.basicConfig(filename='example.log', level=logging.INFO)

        opt_1 = self._get_pyomo_timeseries(result=dict(), demand=load, production=prod, dates=datess,sch = True,
                                      model=m, keys=None)

        return self.get_nudges(chargers=[1, 2, 3, 4], owners=owners, opt_1=opt_1.copy()), opt_1, m.end_cap.value, m

    def general_nudges (self, df, owners):
        # print (owners,"XXXXXXXXX")
        df.set_index("dates", inplace = True)
        names = [f"charger_{x}" for x in range(1, 5)]
        df["nudges"] = [0] * 24

        df["nudges"] = np.where((df.nudges == 0) & (df.index.hour > 7) & (df.index.hour < 21), -1, 0)
        df.loc[df[names].sum(axis=1) > 0, "nudges"] = 1
        # temp.loc[temp.index.hour > 7 &&  temp.index < 20  ,"nudges"] = -1
        return [df["nudges"].values.tolist() for owner in owners]


    def opt_general( self, prod: list = None, load: list = None, init_cap: float = 50,
                     datess: list = None, timestep=60, co2_grid=None,demand=[0],owners = [0],
                      solver='gurobi', verbose=False, solver_path=None):

        # max_dis :  maximum discharge powwer value of the battery (should be negative)
        # max_chg : maximum charging power valu of the battery
        # ch_eff : Battery charghing efficiency (between 1 and 0)
        # dch_eff : Battery discharghing efficiency (between 1 and 0)
        # sd : self-discharge rate of the battery
        # init_cap: initial capacity of the battery
        # timestep : the timestep foor simulation in minutes
        # max_soc : Maximum allowed state of charge (0 <= max_soc <= capacity)
        # min_soc : Minimum allowed state of charge (0 <= min_soc <= capacity)
        # prodd : list containing the PV production values (PV production profile)
        # loadd : list containing the demand profile
        # datess : currently unused
        # ch_1 : Energy consumed by charger 1 during the evaluation period
        # ch_2 : Energy consumed by charger 2 during the evaluation period
        # ch_3 : Energy consumed by charger 3 during the evaluation period
        # ch_4 : Energy consumed by charger 4 during the evaluation period
        # co2_grid : co2 emmisions of the grid in gCO2_eq/kwh
        # verbose: print out optimizer outputs
        # solver: LP solver to use for the optimization "gurobi" is default however change to "glpk" especially if using binder
        # obj: objective of the simulation ["co2" , "sc"] default val ="sc"

        # returns a dataframe with all the calculated porameters

        if sum(prod) > 0:
            l_soc = 0.1
        else:
            l_soc = 0.03
        m = ConcreteModel()
        timestep = timestep / 60

        m.ts = Set(initialize=list(range(0, len(prod))), ordered=True)
        last = m.ts.last()

        m.grid_import = Var(m.ts, domain=NonNegativeReals)
        m.battery_in = Var(m.ts, domain=NegativeReals)
        m.battery_out = Var(m.ts, domain=NonNegativeReals)
        m.battery_energy = Var(m.ts, domain=NonNegativeReals, bounds=(self.min_soc, self.max_soc))
        m.battery_state = Var(m.ts, domain=Binary, bounds=(0, 1))
        m.grid_export = Var(m.ts, domain=NegativeReals)
        m.charger_1 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))
        m.charger_2 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))

        m.charger_3 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))
        m.charger_4 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))

        m.end_cap = Var(domain=NonNegativeReals, bounds=(self.min_soc, self.max_soc))
        m.demand = Var(domain=NonNegativeReals)
        #################################################### Param
        m.self_consumption = Var(domain=NonNegativeReals)
        m.total_co2 = Var(domain=NonNegativeReals)

        #################################################### Rules
        # --------------------------------------------------------
        # ---------------------Battery----------------------------
        # --------------------------------------------------------

        def r_demand(m):
            return (sum(demand) - sum(m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t]  for t in m.ts) == m.demand)

        #######################################################

        def r_ev_charging_time(m, t):
            if datess[t].hour <= 7 or datess[t].hour > 20:
                return (m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t] == 0)
            else:
                return Constraint.Skip
        #
        # def r_charger_total(m):
        #     return (sum(m.charger_1[t]  + m.charger_2[t] + m.charger_3[t] +m. charger_4[t] for t in m.ts) == sum(demand))

        def r_charger_total(m):
            return (sum(m.charger_1[t]  + m.charger_2[t] + m.charger_3[t] +m. charger_4[t] for t in m.ts) <= sum(demand))


        def r_battery_max_powerin(m, t):
            return (m.battery_in[t] >= -self.max_chg * m.battery_state[t])

        def r_battery_max_powerout(m, t):
            return (m.battery_out[t] <= self.max_dis * (m.battery_state[t] - 1))

        def r_battery_energy(m, t):
            if t == 0:
                return m.battery_energy[t] == init_cap
            else:
                return (m.battery_energy[t - 1] * (1 - self.sd) - m.battery_energy[t] ==
                        m.battery_in[t - 1] * timestep * self.ch_eff + m.battery_out[t - 1] * timestep / self.dch_eff)

        def r_battery_min_energy(m, t):
            return (m.battery_energy[t] >= self.min_soc)

        def r_battery_max_energy(m, t):
            return (m.battery_energy[t] <= self.max_soc)

        def r_end_cap(m):
            return (m.battery_energy[last] * (1 - self.sd) - m.battery_in[last] * timestep *self. ch_eff - m.battery_out[last]
                    * timestep / self.dch_eff == m.end_cap)

        def r_battery_max_end_energy(m):
            return (m.end_cap <= self.max_soc)

        def r_battery_min_end_energy(m):
            return (m.end_cap >= self.min_soc * (1 + l_soc))

        # --------------------------------------------------------
        # ------------------Grid imports--------------------------
        # --------------------------------------------------------
        def r_energy_balance(m, t):
            return (prod[t] - (load[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])
                    + m.grid_export[t] + m.grid_import[t] + m.battery_in[t] + m.battery_out[t] == 0)

        def r_grid_export(m, t):
            return (-m.grid_export[t] <= prod[t])

        def r_grid_import(m, t):
            return (m.grid_import[t] <= load[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])

        def r_co2(m):
            if obj == "co2":
                return (sum(m.grid_import[t] * co2_grid[t] for t in m.ts) == m.total_co2)
            else:
                return Constraint.Skip

        def r_self_consumption(m):

            return (m.demand + sum(m.grid_import[t] for t in m.ts) - sum(m.grid_export[t] for t in m.ts)
                    == m.self_consumption)

        # --------------------------------------------------------
        # ---------------------Add To Model-----------------------
        # --------------------------------------------------------
        # Battery
        m.r1 = Constraint(m.ts, rule=r_battery_max_powerin)
        m.r2 = Constraint(m.ts, rule=r_battery_max_powerout)
        m.r3 = Constraint(m.ts, rule=r_battery_energy)

        m.r4 = Constraint(m.ts, rule=r_battery_min_energy)
        m.r5 = Constraint(m.ts, rule=r_battery_max_energy)
        m.r6 = Constraint(rule=r_battery_min_end_energy)

        m.r7 = Constraint(rule=r_battery_max_end_energy)
        m.r8 = Constraint(m.ts, rule=r_energy_balance)
        m.r9 = Constraint(m.ts, rule=r_grid_export)

        m.r10 = Constraint(rule=r_self_consumption)
        m.r11 = Constraint(m.ts, rule=r_grid_import)
        m.r12 = Constraint(m.ts, rule=r_ev_charging_time)

        m.r13 = Constraint(rule=r_charger_total)
        m.r14 = Constraint(rule=r_end_cap)
        # m.r15 = Constraint(rule=r_co2)

        m.r18 = Constraint(rule=r_demand)

        def objective_function(m):
            # if obj == "co2":
            #     return m.total_co2
            # elif obj == "sc":
            return m.self_consumption

        m.objective = Objective(rule=objective_function, sense=minimize)

        # m.write("utils/new.lp")
        with SolverFactory(solver, executable=solver_path) as opt:
            if solver == "glpk":
                opt.options['tmlim'] = 12
            result = opt.solve(m, tee=False)
            if verbose:
                print(result)

        opt_df = self._get_pyomo_timeseries(result=dict(), demand=load, production=prod, sch = False,
                                           dates=datess, model=m, keys=["grid_import", "grid_export", "battery_in",
                                                                        "battery_out", "battery_energy", "charger_1",
                                                                         "charger_2", "charger_3", "charger_4"])
        return m.end_cap.value, m, opt_df, self.general_nudges(df = opt_df.copy(), owners=owners)