import copy
from mesa import  Model
# from  EV_Grenoble.all_agents import *
import MAS_tools.optimizer
# import EV_Grenoble.tools
from  MAS_tools.human_agents import *
from  MAS_tools.EV_agent import *
from  MAS_tools.charger_agent import *
from  MAS_tools.scheduler import *

from MAS_tools.data_collector import *
from  MAS_tools.scheduler  import *
from  MAS_tools.scheduler  import SimultaneousChargers as SC_general,RandomUser as RU_general
# from  EV_Grenoble_general.schedule import SimultaneousChargers,RandomUser as SC_general, RU_general




class EnergyCommunity(Model):

    def __init__(self, agent_info):

        self.schedule = RandomUser(self)
        self.schedule_cs = SimultaneousChargers(self)
        self.num_agents = len(agent_info)
        self.charger_data = {}
        self.container = Data_collection()

        self.schedule_list = []
        self.proj_demand = []
        for i in range (self.num_agents):

            # print (agent_info[i][0]["name"])
            agent_info[i][0]["unique_id"] = i + 101          #EV agent

            agent_info[i][1]["unique_id"] = agent_info[i][0]["unique_id"] - 100     #Human agent

            agent_info[i][1]["ev_id"] = agent_info[i][0]["unique_id"]


            ## type  = 1 : very willing (highly motivated) actor
            agent_human = EV_Owner_Agent(**agent_info[i][1], model = self)
            agent_ev = EV(**agent_info[i][0],model = self)
            # print("TESTED : " , agent_human.schedule)
            self.schedule_list.append(agent_human.schedule)

            # demand = ( agent_ev.soc * agent_ev.batt_cap*0.01 ) -  ( agent_human.daily_dist / agent_ev.km_p_kwh)
            demand = agent_ev.batt_cap - ((agent_ev.soc * agent_ev.batt_cap * 0.01) - ((0.5 * agent_human.daily_dist)
                                                                                     / agent_ev.km_p_kwh))
            agent_human.projected_demand = demand

            # demand = (i+1)**3 / 2 * 5
            self.proj_demand.append(demand)
            # print (agent_)
            self.schedule.add([agent_ev , agent_human])

        for i in range (1,5,1):
            agent_cs = ChargeStation(unique_id= 1000+i,name = "charger_{}".format(i), max_power= 7,model = self)

            self.schedule_cs.add(agent_cs)
        # self.datacollector = DataCollector(agent_reporters={"Available_Range": "avail_range",
        #                                                     "State_of_Charge" : "soc",
        #                                                     "Energy_consumed": "usage",
        #                                                     "Distance" : "distance",
        #                                                     "Battery_energy":"avail_bat"})

    def step(self,current_date = 0, nudges = [0], sim_type = "advanced"):
        '''Advance the model by one step.'''
        # print ("#########################")
        # print (f"current step : { current_date}")
        self.avail_chargers = []
        for cs in self.schedule_cs.agents:
            #check for available charging stations and put them in list of avialable chargers
            if cs.status == 0 :
                self.avail_chargers.append(cs.unique_id)

        # self.datacollector.collect(self)


        self.schedule.step(datetime = current_date, avail_chargers = self.avail_chargers,
                           charger_agents = self.schedule_cs.agents, nudges = nudges,
                           sim_type = sim_type)
        to_charge = []
        for ev , human in self.schedule.agents:
            # nudge = nudges[human.unique_id - 1][current_date]

            # print ("##########  current nudge : ", nudges[human.unique_id - 1][current_date])
            # if current_date == 10:
            #     nudge = nudges [human.unique_id][current_date]
            # elif current_date == 13:
            #     nudge = - human.unique_id
            #     print ("TRUE : ",  nudge)
            # else:
            #     nudge = 0

            # human.nudge = nudge
            # print ("ID : {} , charger ID {}".format(ev.unique_id,ev.charger_id))
            if ev.charger_id > 1000:
                to_charge.append([ev, human])

        self.schedule_cs.step(to_charge = to_charge, datetime=current_date)
        self.container.collector(self)

        def step_general (self, current_date, nudges):
            '''Advance the model by one step.'''
            # print("#########################")
            # print("current step : ", current_date)
            # self.avail_chargers = []

            # for charger in nudges :
            #
            #     if nudges[charger][current_date] == 1:
            #         self.avail_chargers.append[charger]

            # for cs in self.schedule_cs.agents:
            #
            #     if cs.status == 0 :
            #         self.avail_chargers.append(cs.unique_id)

            # self.datacollector.collect(self)

            # self.nudges = nudges
            self.schedule.step(datetime=current_date,
                               charger_agents=self.schedule_cs.agents, nudges=self.nudges)
            to_charge = []
            for ev, human in self.schedule.agents:
                # nudge = nudges[human.unique_id - 1][current_date]

                # print ("##########  current nudge : ", nudges[human.unique_id - 1][current_date])
                # if current_date == 10:
                #     nudge = nudges [human.unique_id][current_date]
                # elif current_date == 13:
                #     nudge = - human.unique_id
                #     print ("TRUE : ",  nudge)
                # else:
                #     nudge = 0

                # human.nudge = nudge
                # print("ID : {} , charger ID {}".format(ev.unique_id, ev.charger_id))
                if ev.charger_id > 1000:
                    to_charge.append([ev, human])

            self.schedule_cs.step(to_charge=to_charge, datetime=current_date)
            self.container.collector(self, charger_agent_reporters=["power"])

    def create_copy(self):
        return copy.deepcopy(self)