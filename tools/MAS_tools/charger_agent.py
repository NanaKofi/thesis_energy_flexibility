from mesa import Agent
import random
import numpy as np

class ChargeStation (Agent):
    def __init__(self,unique_id,name,max_power  ,  model):
        super().__init__(unique_id, model)
        self.agent_name = name
        self.status = 0
        self.max_power = max_power
        self.power = 0

    def step (self,to_charge, datetime, closure , opening):
        """ev_demand is the energy requirements of the vehicle at the current station
        since its one hour time steps for now no need to factor in the time step yet
        """

        closure  = 19
        opening= 7
        self.status = 0
        self.power = 0
        for ev_agent, human in to_charge:
            if ev_agent.soc > 100.00:
                ev_agent.soc = 100
                ev_agent.avail_bat = ev_agent.batt_cap
                ev_agent.avail_range = (ev_agent.soc / 100) * ev_agent.max_range

            ev_agent.demand = (100 - ev_agent.soc) * ev_agent.batt_cap

            if ev_agent.charger_id == self.unique_id :
                if datetime >= closure or datetime < opening :

                    self.status = 0
                    self.power = 0
                    ev_agent.charger_id = 0
                elif datetime >= opening and datetime < closure :
                    if ev_agent.demand < 0:
                        ev_agent.demand = 0
                    self.status = 1
                    self.power  = min (ev_agent.demand , self.max_power)
                    ev_agent.avail_bat += self.power
                    ev_agent.soc =  (ev_agent.avail_bat) / ev_agent.batt_cap
                    ev_agent.soc *= 100


                    ev_agent.avail_range = (ev_agent.soc / 100) * ev_agent.max_range

                if ev_agent.demand == 0 and human.available == 1 :
                    if human.nudge < 0 :
                        options = ["stay", 'move']
                        w = [1 - human.type,  human.type]

                        decision = np.random.choice(options, p=w, size=1)[0]
                        if decision == "move" :
                            self.status = 0

                            ev_agent.charger_id = 0
                        else:
                            self.status = 1
                            ev_agent.charger_id = self.unique_id

                elif ev_agent.demand == 0 and human.available == 0:

                    self.status = 1
                    ev_agent.charger_id = self.unique_id

                ev_agent.demand -= self.power
                ev_agent.met_demand += self.power