import pandas as pd

class Data_collection:

    def __init__(self):
        self.charger_data = dict()

    def collector(self,model, EV_agent_reporter = None, charger_agent_reporters = None ):
        if charger_agent_reporters == None:
            charger_agent_reporters = ["power", "status"]
        for charger in model.schedule_cs.agents:

            for reporter in charger_agent_reporters:

                key = charger.agent_name + "_" + reporter

                if key in self.charger_data.keys():
                    self.charger_data[key].append(getattr(charger, reporter))
                else:
                    self.charger_data[key] = [getattr(charger, reporter)]

        if EV_agent_reporter == None:


            EV_agent_reporter = ["soc", "avail_bat", "avail_range", "demand", "distance", "at_work", "usage", "at_home",
                              "at_other", "available", ]





        for ev, human in model.schedule.agents:
            status = 0
            for reporter in EV_agent_reporter:
                name = ev.agent_name + "_" + reporter
                if reporter == "available":
                    name = human.agent_name + "_" + reporter

                if "at_" in name:
                    name = human.agent_name + "_" + reporter
                    val = getattr(human, reporter)
                    if "work" in name:
                        status += val * 2
                    elif "other" in name:
                        status += val * 3
                    else:
                        status += val * 1

                    if name in self.charger_data.keys():
                        self.charger_data[name].append(val * 1)

                    else:
                        self.charger_data[name] = [val * 1]


                else:

                    if name in self.charger_data.keys() and reporter != "available":

                        self.charger_data[name].append(getattr(ev, reporter))


                    elif name in self.charger_data.keys() and reporter == "available":
                        self.charger_data[name].append(getattr(human, reporter))

                    elif name not in self.charger_data.keys() and reporter == "available":
                        self.charger_data[name] = [getattr(human, reporter)]
                    else:
                        self.charger_data[name] = [getattr(ev, reporter)]

            name = ev.agent_name + "_location"

            if name in self.charger_data.keys():

                self.charger_data[name].append(status)

            else:
                self.charger_data[name] = [status]


    def _getattr(self, name, _object):
        """Turn around arguments of getattr to make it partially callable."""
        return getattr(_object, name, None)


    def get_collector_data(self):
        return pd.DataFrame(self.charger_data)