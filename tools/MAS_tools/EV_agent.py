from mesa import Agent
import random
import numpy as np

class EV (Agent):
    def __init__(self,unique_id,name,max_range, batt_cap,km_p_kwh,min_soc, model):

        super().__init__(unique_id, model)
        self.agent_name = name
        self.owner_id = 0 #todo update with EV owner ID
        self.soc = random.randint(50, 100)  # initial state of charge in %
        self.avail_range = self.soc / 100 * max_range  # available range in km
        self.avail_bat = self.soc / 100 * batt_cap
        self.location = "home"

        self.km_p_kwh = km_p_kwh

        self.batt_cap = batt_cap
        self.max_range = max_range
        self.min_soc = min_soc
        self.demand = 0
        self.met_demand = 0
        self.charger_id = 0


        self.usage = 0
        self.distance = 0




    def step(self,chargers, charger_agents, human):
        self.discharge(chargers = chargers, charger_agents = charger_agents, human = human)
        self.charge(chargers = chargers, charger_agents = charger_agents,human = human )

    def set_charger_status(self,charger_agents, set_point = 0):
        for ch_agent in charger_agents:
            if self.charger_id == ch_agent.unique_id:
                ch_agent.status = set_point

    def discharge (self, chargers , charger_agents,human) :
        options = ["stay", "displace"]
        if self.location == "work" and self.charger_id > 1000 and human.available > 0 :
            if self.soc > 100:
                self.soc = 100

            try:
                if human.nudge < 0 :

                    p =  human.available * (1 - (self.soc/100))
                    p = (p + human.type)/2
                    w = [p , 1-p]
                    decision = np.random.choice(options, p=w, size=1)[0]

            except:
                print(f"Availability : {human.available}\n SOC : {self.soc} \n Type : {human.type}")
                p = human.available * (1 - (self.soc / 100))
                p = (p + human.type) / 2
                w = [p, 1 - p]
                decision = np.random.choice(options, p=w, size=1)[0]
            else:
                decision = "stay"


            # elif human.nudge < 0 and human.type == 1:
            #
            #     w = [1 - human.available, human.available]
            # else :
            #     p = human.available * (1 - (self.soc / 100))
            #     p = (p + human.type) / 2
            #     w = [1 - p,  p]
            # decision = np.random.choice(options, p=w, size=1)[0]

        elif  self.location == "work" and self.charger_id > 1000 and human.available == 0 :
            decision = "stay"
        elif self.location != "work" and self.charger_id >  0 :
            decision = "displace"

        else:
            decision = "stay"


        if decision  ==  "stay":
            pass
        else:
            chargers.append(self.charger_id)
            self.charger_id = 0
            self.set_charger_status(charger_agents=charger_agents, set_point=0)




    def charge(self, chargers, charger_agents , human):

        options = ["charge", 'not_charge']
        if human.nudge >= 1:
            if self.location == "work" and self.charger_id == 0 and human.available > 0 :

                if self.soc > 100:
                    print (f"SOC HIGHER THAN 1: {self.soc}")
                    self.soc = 100
                p = human.available * (1 - (self.soc / 100))
                p = (p + human.type) / 2
                w = [p, 1 - p]
                # if human.nudge > 0 and human.type  == 1:
                #     w = [human.type , 1 - human.type]
                # elif human.nudge <= 0 and human.type  == 1:
                #     w = [1-human.type ,  human.type]
                #
                #
                # elif human.nudge > 0 and human.type < 1 and human.type > 0 :
                #     p = human.available * (1 - (self.soc / 100))
                #     p = (p + human.type) / 2
                #     w = [p, 1 - p ]
                #
                # elif human.nudge <= 0 and human.type < 1 and human.type > 0 :
                #     p = human.available * (1 - (self.soc / 100))
                #     p = (p + human.type) / 2
                #     w = [1-p, p ]
                #
                # elif human.nudge <= 0 and human.type == 0 :
                #     p = human.available * (1 - (self.soc / 100))
                #     p = (p + human.type) / 2
                #     w = [p ,1 - p ]
                #
                # elif human.nudge >= 1 and human.type == 0:
                #     w = [1 - human.type , human.type]

                # print (f"nudge : {human.nudge}, type:  {human.type}")
                decision = np.random.choice(options, p = w, size = 1 )[0]

            # elif self.location == "work" and self.charger_id == 0 and human.available == 0:
            #     decision = "not_charge"
            #
            # elif self.location == "work" and self.charger_id == 1 :
            #     decision = "not_charge"

            else:
                decision = "not_charge"
            # print ("******** : decision : ",decision , "   ******* ", human.nudge)

        else:
            if self.location == "work" and self.charger_id == 0 and human.available > 0:

                if self.soc > 100:
                    print(f"SOC HIGHER THAN 1: {self.soc}")
                    self.soc = 100
                p = human.available * (1 - (self.soc / 100))
                p = (p + human.type) / 2
                w = [1 -p,  p]

                decision = np.random.choice(options, p=w, size=1)[0]

            else:
                decision = "not_charge"

        if decision == "charge" and len(chargers) > 0:
            # print("Charging took place",chargers, human.nudge)
            # human.available = 1
            self.charger_id = 1000+ int(round(human.nudge,0))
            if self.charger_id not in chargers:
                self.charger_id = random.choice(chargers)
            chargers.remove(self.charger_id)
            self.set_charger_status(charger_agents=charger_agents, set_point=1)
        else:
            pass

        # elif self.location == "work" and self.charger_id == 0 and human.available == 1 and human.type == 0:
        #     if human.nudge >= 1:
        #         w = [ human.type , 1-human.type]
        #         # p = (0.35 * (100 - self.soc)) / 100
        #         # w = [ p,1- p]
        #     else:
        #         # w = [0.5,0.5]
        #         # w = [0.3, 0.7]
        #         # p = (0.30 * (100 - self.soc)) / 100
        #         p =  self.soc/ 100
        #         w  = [1-p , p]
        #
        #     options = ["charge", 'not_charge']
        #     decision = np.random.choice(options, p=w, size=1)[0]
        #
        #     if decision == "charge" and len(chargers) > 0:
        #         print("Charging took place")
        #         # human.available = 1
        #         self.charger_id = random.choice(chargers)
        #         chargers.remove(self.charger_id)
        #         self.set_charger_status(charger_agents=charger_agents, set_point=1)
        # elif self.location == "work" and self.soc == 100 and self.charger_id >= 1000:


            # if human.available == 1:
            #     self.charger_id = 0
            #     self.set_charger_status(charger_agents=charger_agents,  set_point=0)

        # elif self.location == "work" and self.charger_id >= 1000 and human.available == 1 :
        #
        #     print ("NUDGE : ", human.nudge)
        #     if human.nudge < 0 and human.type == 1 :
        #         self.charger_id = 0
        #         self.set_charger_status(charger_agents=charger_agents, set_point=0)
        #
        #     elif human.nudge < 0 and human.type == 0 :
        #         p = (0.10 * (100 - self.soc)) / 100
        #         w = [p, 1-p]
        #
        #         options = ["charge", 'not_charge']
        #         decision = np.random.choice(options, p=w, size=1)[0]
        #
        #     elif human.nudge == 0 and human.type == 0:
        #         options = ["charge", 'not_charge']
        #         p = (0.65 * (100 - self.soc)) / 100
        #         decision = np.random.choice(options, p=[1-p,p], size=1)[0]
        #
        #         if decision == "charge" :
        #             pass
        #         else:
        #             self.charger_id = 0
        #             self.set_charger_status(charger_agents=charger_agents, set_point=0)
        #
        #
        #
        # elif self.location == "work" and self.charger_id >= 1000 and human.available == 0 :
        #     pass
        #     # print ("NUDGE : ", human.nudge)
        #     # if human.nudge < 0 and human.type == 1 :
        #     #     pass
        #     # #     self.charger_id = 0
        #     # #     self.set_charger_status(charger_agents=charger_agents, set_point=0)
        #     #
        #     # elif human.nudge < 0 and human.type == 0 :
        #     #     pass
        #     #
        #     # elif human.nudge == 0 and human.type == 0:
        #     #     options = ["charge", 'not_charge']
        #     #     decision = np.random.choice(options, p=[0.5,0.5], size=1)[0]
        #     #
        #     #     if decision == "charge" :
        #     #         pass
        #     #     else:
        #     #         self.charger_id = 0
        #     #         self.set_charger_status(charger_agents=charger_agents, set_point=0)
        #
        #
        # elif self.location != "work" and self.charger_id >= 1000:
        #     # human.available == 1
        #     self.charger_id = 0
        #     self.set_charger_status(charger_agents=charger_agents,  set_point=0)
        #
        #     # Charge at work
        # elif self.location != "work" :
        #     pass
            # if self.soc > self.min_soc:
            #     options = ["charge", 'not_charge']
            #     w = [0.1, 0.9]
            # else:
            #     options = ["charge", 'not_charge']
            #     w = [0.4, 0.6]

            # decision = np.random.choice(options, p=w, size=1)[0]
            # if decision == "charge":
            #     self.demand = 0
            #     self.avail_bat = self.batt_cap
            #     self.avail_range = self.max_range

            self.charger_id = 0

    # self.at_work = False
    # self.at_home = True
    # self.at_other = False