import plotly.express as px
import plotly.subplots as sp


def plotter (capacity_df, a= 0.7, b= 0.54, title = None) :
    d = round (capacity_df["direct only"].loc[a],2)
    di = round (capacity_df["direct & indirect"].loc[a],2)

    d_ = round (capacity_df["direct only"].loc[b],2)
    di_ = round (capacity_df["direct & indirect"].loc[b],2)

    layout = {
       "font":{"size" : 20},"width" : 1200, "height" : 650, #"symbol" : 'cross',
    #     "marker" : {"size" : 15, "symbol" : 'cross'},
         "annotations":[
    #          { "showarrow" : True, "x" : 0.42, "y" : 1, "xref":"x", "yref":"y",
    #             "text" : "Natural self-conumption", "xshift":0, "arrowhead":2, "xanchor":"center",
    #             "ax" : 0, "ay" : -92,"yanchor":"top", "arrowsize" : 3, "opacity":1},

                 { "showarrow" : True, "x" : 0.54, "y" : d_, "xref":"x", "yref":"y",
                "text" : f"Direct only : {d_} kWh ", "xshift":0, "arrowhead":2, "xanchor":"center",
                "ax" : 0, "ay" : -75, "yanchor":"top", "arrowsize" : 3, "opacity":1},

                { "showarrow" : False, "x" : b, "y" : d_+70, "xref":"x", "yref":"y",
                "text" : f"Direct + Indirect : {di_} kWh", "xshift":0, "arrowhead":2, "xanchor":"center",
                "ax" : -120, "ay" : 0,"yanchor":"top", "arrowsize" : 3, "opacity":1},


                { "showarrow" : True, "x" : a, "y" : d+15, "xref":"x", "yref":"y",
                "text" : f"Direct only : {d} kWh ", "xshift":0, "arrowhead":2, "xanchor":"center",
                "ax" : 0, "ay" : -120,"yanchor":"top", "arrowsize" : 3, "opacity":1},

                { "showarrow" : False, "x" : 0.72, "y" : d+120, "xref":"x", "yref":"y",
                "text" : f"Direct + Indirect : {di} kWh", "xshift":0, "arrowhead":2, "xanchor":"center",
                "ax" : -140, "ay" : 100,"yanchor":"top", "arrowsize" : 3, "opacity":1},
                      ],
        'xaxis': {'zerolinewidth': 1,
                  'zerolinecolor':'black'
                 },
        "yaxis": {'zerolinewidth': 1,
                  'zerolinecolor':'black',
                   "side": 'left',
                  "range": [0, 330],"dtick": 30,
                  "title_text" : "Battery Capacity [kWh]",
        },
        "yaxis2": {'zerolinewidth': 1,
                  'zerolinecolor':'black',
                   "side": 'right',
                  "range": [0, 110], "dtick": 10,
                  "title_text" : "Percent Reduction [%]",
        },

        "legend" : {"tracegroupgap":4 ,"font_size": 20,
                    "title" : "Flexibility"}
    }



    fig = px.line(capacity_df[["direct & indirect", "direct only"]], template = "plotly_white", markers=True)
    fig.update_layout(layout , hovermode = "x" )
    fig['data'][1]['marker']['size'] = 11
    fig['data'][0]['marker']['symbol'] = 'square'
    fig['data'][0]['marker']['size'] = 9


    capacity_df["difference"] =((capacity_df["direct only"] - capacity_df["direct & indirect"])
                                / capacity_df["direct only"]) *100

    fig1 = px.line(capacity_df,x=capacity_df.index , y = "difference",template = "plotly_white", markers=True,
                  color_discrete_sequence=["green"])
    fig1.update_layout(hovermode = "x")
    fig1.update_yaxes(title_text="percentage change")
    fig1.data[-1].showlegend = True
    fig1.data[-1].name = "Percentage Reduction"
    fig1['data'][0]['marker']['symbol'] = 'star'
    fig1['data'][0]['marker']['size'] = 9
    fig1.update_traces(yaxis="y2")


    subfig = sp.make_subplots(specs=[[{"secondary_y": True}]])
    subfig.add_traces(fig.data + fig1.data)
    subfig.layout.xaxis.title="Self-consumption"


    subfig.update_yaxes( secondary_y=False)
    subfig.update_yaxes( color = "green" , secondary_y=True)
    subfig.update_layout(layout, template = "plotly_white", title = title)

    return subfig