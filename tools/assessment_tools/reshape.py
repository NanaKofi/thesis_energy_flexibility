import pandas as pd
import numpy as np





def reshape_data(feature_size: int = 24, horizon:int  = 1, df:pd.DataFrame = None  ):

    """
    Method to reshape process data into a numpy array of m * n shape
    :param feature_size: size of input to be used for prediction (given in hours)
    :param horizon :  number of hours to predict
    :param df :  pandas dataframe with feature columns

    :return: numpy array of m * n shape
    """
    new_data = []
    start = df.index[0]
    observation_info = {}

    for i in range (int (len(df)/horizon)):
        new_data.append(df.loc[start: start + timedelta (hours = feature_size)].copy().values)
        observation_info[i] = [start, start + timedelta(hours=feature_size)]
        start = start + timedelta (hours = horizon)

    new_data = np.array(new_data)

    shp = new_data.shape

    new_data = new_data.reshape(shp[0], shp[1] * shp[2])

    return new_data,observation_info


def calendar(df) :
    df = df.reset_index()

    df['Year'] = df['datetime'].dt.year
    df['Month_name'] = df['datetime'].dt.month_name()
    df['Weekday'] = df['datetime'].dt.day_name()
    df['Day_number'] = df['datetime'].dt.day
    df['Hour'] = df['datetime'].dt.hour
    df["week"] =  df['datetime'].dt.isocalendar().week
    df["date"] =  df['datetime'].dt.date

    df = df.set_index('datetime')

    df['Calendar'] = 1
    ### Public Holidays
    fr = holidays.FR()

    # df.loc[df["date"] in  fr, 'calendar'] = 0
    for d, ind in zip(df["date"] , df.index) :

        if d in fr :
            df.loc[ind,"calendar"] = 0


    ### weekends
    df.loc[(df['Weekday'] == 'Saturday'), 'Calendar'] = 0
    df.loc[(df['Weekday'] == 'Sunday'), 'Calendar'] = 0

    ####COVID Lockdown:
    df.loc[(df['Month_name'] == "March") & (df["Day_number"] >= 16) & (df["Year"] == 2020), 'Calendar'] = 0
    df.loc[(df['Month_name'] == "April") & (df["Year"] == 2020), 'Calendar'] = 0
    df.loc[(df['Month_name'] == "May") & (df["Day_number"] <= 11) & (df["Year"] == 2020), 'Calendar'] = 0
    df.loc[(df['Month_name'] == 'June') & (df['Year'] == 2020), 'Calendar'] = 0
    df.loc[(df['Month_name'] == 'September') & (df['Year'] == 2020), 'Calendar'] = 0
    #df.loc[(df['Month_name'] == "October") & (df["Day_number"] >= 28) & (df["Year"] == 2020), 'Calendar'] = 0

    ## Lab Holidays
    df.loc[(df['week'] >=51 ) , 'Calendar'] = 0
    df.loc[(df['Month_name'] == "July") & (df["Day_number"] == 31 ), 'Calendar'] = 0
    df.loc[(df['Month_name'] == "August") & (df["Day_number"] <= 15), 'Calendar'] = 0
    # print (df['Hour'])
    df.loc[(df['Hour'] > 20), 'Calendar'] = 0
    df.loc[(df['Hour'] < 7), 'Calendar'] = 0
    try:
        df.drop(columns=['Year',"week", 'Month_name', 'Weekday', 'Day_number', 'Hour', 'TMP'], inplace=True)

    except KeyError:
        df.drop(columns=['Year', "week", 'Month_name', 'Weekday', 'Day_number', 'Hour'], inplace=True)

    return df