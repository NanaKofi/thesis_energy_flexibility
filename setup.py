import setuptools
from setuptools import setup

with open("requirements.txt", "r") as f:
    requirements = f.read().split()

setup(
    name="tools",
    version="0.1",
    description="thesis on the interaction between direct and indirect energy flexibility in buildings",
    url="none",
    author="Nana Kofi",
    author_email=["nana-kofi-baabu.twum_duah@grenoble-inp.fr"],
    keywords="Optimization, indirect flexibility, PV, battery,  Electric vehicles",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    python_requires=">=3.7.12",
)
