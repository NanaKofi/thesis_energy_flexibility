# Data description
- Power profile for GrEn-ER electrical (engineering school building in Grenoble) PV production, consumption and 4 electric vehicles chargers in 2021, with a one hour time step.
- Unit: kW

## Data licence

### Name
Creative Commons Attribution 4.0 International (CC BY 4.0)

### URL
https://creativecommons.org/licenses/by/4.0/

### Instructions
You are free: *To Share, To Create, To Adapt*; As long as you: *Attribute*  

### Files
Predis_data_2021.csv

### Copyright
© Laboratoire de génie électrique de Grenoble (G2Elab)
