*SENSE:Minimize
NAME          Autoconsommation
ROWS
 N  maximize_self_consumption
 E  battery_calc_energy_0
 E  energybalance_0
 G  Battery_max_charging_0
 G  Import_constraint0
 L  Battery_max_discharging_0
 L  Export_constraint_0
 E  _Battery_power_calc_0
 G  min_Import_0
 L  min_Export_0
 L  Battery_max_SOC_0
 G  Battery_min_SOC_0
 L  min_Imports_0
 E  energybalance_1
 G  Battery_max_charging_1
 G  Import_constraint1
 L  Battery_max_discharging_1
 L  Export_constraint_1
 E  _Battery_power_calc_1
 G  min_Import_1
 L  min_Export_1
 L  Battery_max_SOC_1
 G  Battery_min_SOC_1
 L  min_Imports_1
 E  energybalance_2
 G  Battery_max_charging_2
 G  Import_constraint2
 L  Battery_max_discharging_2
 L  Export_constraint_2
 E  _Battery_power_calc_2
 G  min_Import_2
 L  min_Export_2
 L  Battery_max_SOC_2
 G  Battery_min_SOC_2
 L  min_Imports_2
 E  energybalance_3
 G  Battery_max_charging_3
 G  Import_constraint3
 L  Battery_max_discharging_3
 L  Export_constraint_3
 E  _Battery_power_calc_3
 G  min_Import_3
 L  min_Export_3
 L  Battery_max_SOC_3
 G  Battery_min_SOC_3
 L  min_Imports_3
 E  energybalance_4
 G  Battery_max_charging_4
 G  Import_constraint4
 L  Battery_max_discharging_4
 L  Export_constraint_4
 E  _Battery_power_calc_4
 G  min_Import_4
 L  min_Export_4
 L  Battery_max_SOC_4
 G  Battery_min_SOC_4
 L  min_Imports_4
 E  energybalance_5
 G  Battery_max_charging_5
 G  Import_constraint5
 L  Battery_max_discharging_5
 L  Export_constraint_5
 E  _Battery_power_calc_5
 G  min_Import_5
 L  min_Export_5
 L  Battery_max_SOC_5
 G  Battery_min_SOC_5
 L  min_Imports_5
 E  energybalance_6
 G  Battery_max_charging_6
 G  Import_constraint6
 L  Battery_max_discharging_6
 L  Export_constraint_6
 E  _Battery_power_calc_6
 G  min_Import_6
 L  min_Export_6
 L  Battery_max_SOC_6
 G  Battery_min_SOC_6
 L  min_Imports_6
 E  energybalance_7
 G  Battery_max_charging_7
 G  Import_constraint7
 L  Battery_max_discharging_7
 L  Export_constraint_7
 E  _Battery_power_calc_7
 G  min_Import_7
 L  min_Export_7
 L  Battery_max_SOC_7
 G  Battery_min_SOC_7
 L  min_Imports_7
 E  energybalance_8
 G  Battery_max_charging_8
 G  Import_constraint8
 L  Battery_max_discharging_8
 L  Export_constraint_8
 E  _Battery_power_calc_8
 G  min_Import_8
 L  min_Export_8
 L  Battery_max_SOC_8
 G  Battery_min_SOC_8
 L  min_Imports_8
 E  energybalance_9
 G  Battery_max_charging_9
 G  Import_constraint9
 L  Battery_max_discharging_9
 L  Export_constraint_9
 E  _Battery_power_calc_9
 G  min_Import_9
 L  min_Export_9
 L  Battery_max_SOC_9
 G  Battery_min_SOC_9
 L  min_Imports_9
 E  energybalance_10
 G  Battery_max_charging_10
 G  Import_constraint10
 L  Battery_max_discharging_10
 L  Export_constraint_10
 E  _Battery_power_calc_10
 G  min_Import_10
 L  min_Export_10
 L  Battery_max_SOC_10
 G  Battery_min_SOC_10
 L  min_Imports_10
 E  energybalance_11
 G  Battery_max_charging_11
 G  Import_constraint11
 L  Battery_max_discharging_11
 L  Export_constraint_11
 E  _Battery_power_calc_11
 G  min_Import_11
 L  min_Export_11
 L  Battery_max_SOC_11
 G  Battery_min_SOC_11
 L  min_Imports_11
 E  energybalance_12
 G  Battery_max_charging_12
 G  Import_constraint12
 L  Battery_max_discharging_12
 L  Export_constraint_12
 E  _Battery_power_calc_12
 G  min_Import_12
 L  min_Export_12
 L  Battery_max_SOC_12
 G  Battery_min_SOC_12
 L  min_Imports_12
 L  min_Exports_12
 E  energybalance_13
 G  Battery_max_charging_13
 G  Import_constraint13
 L  Battery_max_discharging_13
 L  Export_constraint_13
 E  _Battery_power_calc_13
 G  min_Import_13
 L  min_Export_13
 L  Battery_max_SOC_13
 G  Battery_min_SOC_13
 L  min_Imports_13
 E  energybalance_14
 G  Battery_max_charging_14
 G  Import_constraint14
 L  Battery_max_discharging_14
 L  Export_constraint_14
 E  _Battery_power_calc_14
 G  min_Import_14
 L  min_Export_14
 L  Battery_max_SOC_14
 G  Battery_min_SOC_14
 L  min_Imports_14
 E  energybalance_15
 G  Battery_max_charging_15
 G  Import_constraint15
 L  Battery_max_discharging_15
 L  Export_constraint_15
 E  _Battery_power_calc_15
 G  min_Import_15
 L  min_Export_15
 L  Battery_max_SOC_15
 G  Battery_min_SOC_15
 L  min_Imports_15
 E  energybalance_16
 G  Battery_max_charging_16
 G  Import_constraint16
 L  Battery_max_discharging_16
 L  Export_constraint_16
 E  _Battery_power_calc_16
 G  min_Import_16
 L  min_Export_16
 L  Battery_max_SOC_16
 G  Battery_min_SOC_16
 L  min_Imports_16
 E  energybalance_17
 G  Battery_max_charging_17
 G  Import_constraint17
 L  Battery_max_discharging_17
 L  Export_constraint_17
 E  _Battery_power_calc_17
 G  min_Import_17
 L  min_Export_17
 L  Battery_max_SOC_17
 G  Battery_min_SOC_17
 L  min_Imports_17
 E  energybalance_18
 G  Battery_max_charging_18
 G  Import_constraint18
 L  Battery_max_discharging_18
 L  Export_constraint_18
 E  _Battery_power_calc_18
 G  min_Import_18
 L  min_Export_18
 L  Battery_max_SOC_18
 G  Battery_min_SOC_18
 L  min_Imports_18
 E  energybalance_19
 G  Battery_max_charging_19
 G  Import_constraint19
 L  Battery_max_discharging_19
 L  Export_constraint_19
 E  _Battery_power_calc_19
 G  min_Import_19
 L  min_Export_19
 L  Battery_max_SOC_19
 G  Battery_min_SOC_19
 L  min_Imports_19
 E  energybalance_20
 G  Battery_max_charging_20
 G  Import_constraint20
 L  Battery_max_discharging_20
 L  Export_constraint_20
 E  _Battery_power_calc_20
 G  min_Import_20
 L  min_Export_20
 L  Battery_max_SOC_20
 G  Battery_min_SOC_20
 L  min_Imports_20
 E  energybalance_21
 G  Battery_max_charging_21
 G  Import_constraint21
 L  Battery_max_discharging_21
 L  Export_constraint_21
 E  _Battery_power_calc_21
 G  min_Import_21
 L  min_Export_21
 L  Battery_max_SOC_21
 G  Battery_min_SOC_21
 L  min_Imports_21
 E  energybalance_22
 G  Battery_max_charging_22
 G  Import_constraint22
 L  Battery_max_discharging_22
 L  Export_constraint_22
 E  _Battery_power_calc_22
 G  min_Import_22
 L  min_Export_22
 L  Battery_max_SOC_22
 G  Battery_min_SOC_22
 L  min_Imports_22
 E  energybalance_23
 G  Battery_max_charging_23
 G  Import_constraint23
 L  Battery_max_discharging_23
 L  Export_constraint_23
 E  _Battery_power_calc_23
 G  min_Import_23
 L  min_Export_23
 L  Battery_max_SOC_23
 G  Battery_min_SOC_23
 L  min_Imports_23
 G  final_energy_min
 L  Final_energy_max
 E  battery_energy_constraint_0
 E  battery_energy_constraint_1
 E  battery_energy_constraint_2
 E  battery_energy_constraint_3
 E  battery_energy_constraint_4
 E  battery_energy_constraint_5
 E  battery_energy_constraint_6
 E  battery_energy_constraint_7
 E  battery_energy_constraint_8
 E  battery_energy_constraint_9
 E  battery_energy_constraint_10
 E  battery_energy_constraint_11
 E  battery_energy_constraint_12
 E  battery_energy_constraint_13
 E  battery_energy_constraint_14
 E  battery_energy_constraint_15
 E  battery_energy_constraint_16
 E  battery_energy_constraint_17
 E  battery_energy_constraint_18
 E  battery_energy_constraint_19
 E  battery_energy_constraint_20
 E  battery_energy_constraint_21
 E  battery_energy_constraint_22
COLUMNS
    Bat_en_0  battery_calc_energy_0   1.000000000000e+00
    Bat_en_0  Battery_max_SOC_0   1.000000000000e+00
    Bat_en_0  Battery_min_SOC_0   1.000000000000e+00
    Bat_en_0  battery_energy_constraint_0   1.000000000000e+00
    Bat_en_1  Battery_max_SOC_1   1.000000000000e+00
    Bat_en_1  Battery_min_SOC_1   1.000000000000e+00
    Bat_en_1  battery_energy_constraint_0  -1.000000000000e+00
    Bat_en_1  battery_energy_constraint_1   1.000000000000e+00
    Bat_en_10  Battery_max_SOC_10   1.000000000000e+00
    Bat_en_10  Battery_min_SOC_10   1.000000000000e+00
    Bat_en_10  battery_energy_constraint_9  -1.000000000000e+00
    Bat_en_10  battery_energy_constraint_10   1.000000000000e+00
    Bat_en_11  Battery_max_SOC_11   1.000000000000e+00
    Bat_en_11  Battery_min_SOC_11   1.000000000000e+00
    Bat_en_11  battery_energy_constraint_10  -1.000000000000e+00
    Bat_en_11  battery_energy_constraint_11   1.000000000000e+00
    Bat_en_12  Battery_max_SOC_12   1.000000000000e+00
    Bat_en_12  Battery_min_SOC_12   1.000000000000e+00
    Bat_en_12  battery_energy_constraint_11  -1.000000000000e+00
    Bat_en_12  battery_energy_constraint_12   1.000000000000e+00
    Bat_en_13  Battery_max_SOC_13   1.000000000000e+00
    Bat_en_13  Battery_min_SOC_13   1.000000000000e+00
    Bat_en_13  battery_energy_constraint_12  -1.000000000000e+00
    Bat_en_13  battery_energy_constraint_13   1.000000000000e+00
    Bat_en_14  Battery_max_SOC_14   1.000000000000e+00
    Bat_en_14  Battery_min_SOC_14   1.000000000000e+00
    Bat_en_14  battery_energy_constraint_13  -1.000000000000e+00
    Bat_en_14  battery_energy_constraint_14   1.000000000000e+00
    Bat_en_15  Battery_max_SOC_15   1.000000000000e+00
    Bat_en_15  Battery_min_SOC_15   1.000000000000e+00
    Bat_en_15  battery_energy_constraint_14  -1.000000000000e+00
    Bat_en_15  battery_energy_constraint_15   1.000000000000e+00
    Bat_en_16  Battery_max_SOC_16   1.000000000000e+00
    Bat_en_16  Battery_min_SOC_16   1.000000000000e+00
    Bat_en_16  battery_energy_constraint_15  -1.000000000000e+00
    Bat_en_16  battery_energy_constraint_16   1.000000000000e+00
    Bat_en_17  Battery_max_SOC_17   1.000000000000e+00
    Bat_en_17  Battery_min_SOC_17   1.000000000000e+00
    Bat_en_17  battery_energy_constraint_16  -1.000000000000e+00
    Bat_en_17  battery_energy_constraint_17   1.000000000000e+00
    Bat_en_18  Battery_max_SOC_18   1.000000000000e+00
    Bat_en_18  Battery_min_SOC_18   1.000000000000e+00
    Bat_en_18  battery_energy_constraint_17  -1.000000000000e+00
    Bat_en_18  battery_energy_constraint_18   1.000000000000e+00
    Bat_en_19  Battery_max_SOC_19   1.000000000000e+00
    Bat_en_19  Battery_min_SOC_19   1.000000000000e+00
    Bat_en_19  battery_energy_constraint_18  -1.000000000000e+00
    Bat_en_19  battery_energy_constraint_19   1.000000000000e+00
    Bat_en_2  Battery_max_SOC_2   1.000000000000e+00
    Bat_en_2  Battery_min_SOC_2   1.000000000000e+00
    Bat_en_2  battery_energy_constraint_1  -1.000000000000e+00
    Bat_en_2  battery_energy_constraint_2   1.000000000000e+00
    Bat_en_20  Battery_max_SOC_20   1.000000000000e+00
    Bat_en_20  Battery_min_SOC_20   1.000000000000e+00
    Bat_en_20  battery_energy_constraint_19  -1.000000000000e+00
    Bat_en_20  battery_energy_constraint_20   1.000000000000e+00
    Bat_en_21  Battery_max_SOC_21   1.000000000000e+00
    Bat_en_21  Battery_min_SOC_21   1.000000000000e+00
    Bat_en_21  battery_energy_constraint_20  -1.000000000000e+00
    Bat_en_21  battery_energy_constraint_21   1.000000000000e+00
    Bat_en_22  Battery_max_SOC_22   1.000000000000e+00
    Bat_en_22  Battery_min_SOC_22   1.000000000000e+00
    Bat_en_22  battery_energy_constraint_21  -1.000000000000e+00
    Bat_en_22  battery_energy_constraint_22   1.000000000000e+00
    Bat_en_23  Battery_max_SOC_23   1.000000000000e+00
    Bat_en_23  Battery_min_SOC_23   1.000000000000e+00
    Bat_en_23  final_energy_min   1.000000000000e+00
    Bat_en_23  Final_energy_max   1.000000000000e+00
    Bat_en_23  battery_energy_constraint_22  -1.000000000000e+00
    Bat_en_3  Battery_max_SOC_3   1.000000000000e+00
    Bat_en_3  Battery_min_SOC_3   1.000000000000e+00
    Bat_en_3  battery_energy_constraint_2  -1.000000000000e+00
    Bat_en_3  battery_energy_constraint_3   1.000000000000e+00
    Bat_en_4  Battery_max_SOC_4   1.000000000000e+00
    Bat_en_4  Battery_min_SOC_4   1.000000000000e+00
    Bat_en_4  battery_energy_constraint_3  -1.000000000000e+00
    Bat_en_4  battery_energy_constraint_4   1.000000000000e+00
    Bat_en_5  Battery_max_SOC_5   1.000000000000e+00
    Bat_en_5  Battery_min_SOC_5   1.000000000000e+00
    Bat_en_5  battery_energy_constraint_4  -1.000000000000e+00
    Bat_en_5  battery_energy_constraint_5   1.000000000000e+00
    Bat_en_6  Battery_max_SOC_6   1.000000000000e+00
    Bat_en_6  Battery_min_SOC_6   1.000000000000e+00
    Bat_en_6  battery_energy_constraint_5  -1.000000000000e+00
    Bat_en_6  battery_energy_constraint_6   1.000000000000e+00
    Bat_en_7  Battery_max_SOC_7   1.000000000000e+00
    Bat_en_7  Battery_min_SOC_7   1.000000000000e+00
    Bat_en_7  battery_energy_constraint_6  -1.000000000000e+00
    Bat_en_7  battery_energy_constraint_7   1.000000000000e+00
    Bat_en_8  Battery_max_SOC_8   1.000000000000e+00
    Bat_en_8  Battery_min_SOC_8   1.000000000000e+00
    Bat_en_8  battery_energy_constraint_7  -1.000000000000e+00
    Bat_en_8  battery_energy_constraint_8   1.000000000000e+00
    Bat_en_9  Battery_max_SOC_9   1.000000000000e+00
    Bat_en_9  Battery_min_SOC_9   1.000000000000e+00
    Bat_en_9  battery_energy_constraint_8  -1.000000000000e+00
    Bat_en_9  battery_energy_constraint_9   1.000000000000e+00
    Bat_pow_0  _Battery_power_calc_0  -1.000000000000e+00
    Bat_pow_1  _Battery_power_calc_1  -1.000000000000e+00
    Bat_pow_10  _Battery_power_calc_10  -1.000000000000e+00
    Bat_pow_11  _Battery_power_calc_11  -1.000000000000e+00
    Bat_pow_12  _Battery_power_calc_12  -1.000000000000e+00
    Bat_pow_13  _Battery_power_calc_13  -1.000000000000e+00
    Bat_pow_14  _Battery_power_calc_14  -1.000000000000e+00
    Bat_pow_15  _Battery_power_calc_15  -1.000000000000e+00
    Bat_pow_16  _Battery_power_calc_16  -1.000000000000e+00
    Bat_pow_17  _Battery_power_calc_17  -1.000000000000e+00
    Bat_pow_18  _Battery_power_calc_18  -1.000000000000e+00
    Bat_pow_19  _Battery_power_calc_19  -1.000000000000e+00
    Bat_pow_2  _Battery_power_calc_2  -1.000000000000e+00
    Bat_pow_20  _Battery_power_calc_20  -1.000000000000e+00
    Bat_pow_21  _Battery_power_calc_21  -1.000000000000e+00
    Bat_pow_22  _Battery_power_calc_22  -1.000000000000e+00
    Bat_pow_23  _Battery_power_calc_23  -1.000000000000e+00
    Bat_pow_3  _Battery_power_calc_3  -1.000000000000e+00
    Bat_pow_4  _Battery_power_calc_4  -1.000000000000e+00
    Bat_pow_5  _Battery_power_calc_5  -1.000000000000e+00
    Bat_pow_6  _Battery_power_calc_6  -1.000000000000e+00
    Bat_pow_7  _Battery_power_calc_7  -1.000000000000e+00
    Bat_pow_8  _Battery_power_calc_8  -1.000000000000e+00
    Bat_pow_9  _Battery_power_calc_9  -1.000000000000e+00
    MARK      'MARKER'                 'INTORG'
    Bat_st_0  Battery_max_charging_0   4.000000000000e+01
    Bat_st_0  Battery_max_discharging_0   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_1  Battery_max_charging_1   4.000000000000e+01
    Bat_st_1  Battery_max_discharging_1   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_10  Battery_max_charging_10   4.000000000000e+01
    Bat_st_10  Battery_max_discharging_10   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_11  Battery_max_charging_11   4.000000000000e+01
    Bat_st_11  Battery_max_discharging_11   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_12  Battery_max_charging_12   4.000000000000e+01
    Bat_st_12  Battery_max_discharging_12   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_13  Battery_max_charging_13   4.000000000000e+01
    Bat_st_13  Battery_max_discharging_13   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_14  Battery_max_charging_14   4.000000000000e+01
    Bat_st_14  Battery_max_discharging_14   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_15  Battery_max_charging_15   4.000000000000e+01
    Bat_st_15  Battery_max_discharging_15   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_16  Battery_max_charging_16   4.000000000000e+01
    Bat_st_16  Battery_max_discharging_16   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_17  Battery_max_charging_17   4.000000000000e+01
    Bat_st_17  Battery_max_discharging_17   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_18  Battery_max_charging_18   4.000000000000e+01
    Bat_st_18  Battery_max_discharging_18   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_19  Battery_max_charging_19   4.000000000000e+01
    Bat_st_19  Battery_max_discharging_19   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_2  Battery_max_charging_2   4.000000000000e+01
    Bat_st_2  Battery_max_discharging_2   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_20  Battery_max_charging_20   4.000000000000e+01
    Bat_st_20  Battery_max_discharging_20   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_21  Battery_max_charging_21   4.000000000000e+01
    Bat_st_21  Battery_max_discharging_21   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_22  Battery_max_charging_22   4.000000000000e+01
    Bat_st_22  Battery_max_discharging_22   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_23  Battery_max_charging_23   4.000000000000e+01
    Bat_st_23  Battery_max_discharging_23   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_3  Battery_max_charging_3   4.000000000000e+01
    Bat_st_3  Battery_max_discharging_3   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_4  Battery_max_charging_4   4.000000000000e+01
    Bat_st_4  Battery_max_discharging_4   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_5  Battery_max_charging_5   4.000000000000e+01
    Bat_st_5  Battery_max_discharging_5   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_6  Battery_max_charging_6   4.000000000000e+01
    Bat_st_6  Battery_max_discharging_6   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_7  Battery_max_charging_7   4.000000000000e+01
    Bat_st_7  Battery_max_discharging_7   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_8  Battery_max_charging_8   4.000000000000e+01
    Bat_st_8  Battery_max_discharging_8   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    MARK      'MARKER'                 'INTORG'
    Bat_st_9  Battery_max_charging_9   4.000000000000e+01
    Bat_st_9  Battery_max_discharging_9   4.000000000000e+01
    MARK      'MARKER'                 'INTEND'
    Export_0  energybalance_0   1.000000000000e+00
    Export_0  min_Export_0   1.000000000000e+00
    Export_0  maximize_self_consumption  -1.000000000000e+00
    Export_1  energybalance_1   1.000000000000e+00
    Export_1  min_Export_1   1.000000000000e+00
    Export_1  maximize_self_consumption  -1.000000000000e+00
    Export_10  energybalance_10   1.000000000000e+00
    Export_10  min_Export_10   1.000000000000e+00
    Export_10  maximize_self_consumption  -1.000000000000e+00
    Export_11  energybalance_11   1.000000000000e+00
    Export_11  min_Export_11   1.000000000000e+00
    Export_11  maximize_self_consumption  -1.000000000000e+00
    Export_12  energybalance_12   1.000000000000e+00
    Export_12  min_Export_12   1.000000000000e+00
    Export_12  min_Exports_12  -1.000000000000e+00
    Export_12  maximize_self_consumption  -1.000000000000e+00
    Export_13  energybalance_13   1.000000000000e+00
    Export_13  min_Export_13   1.000000000000e+00
    Export_13  maximize_self_consumption  -1.000000000000e+00
    Export_14  energybalance_14   1.000000000000e+00
    Export_14  min_Export_14   1.000000000000e+00
    Export_14  maximize_self_consumption  -1.000000000000e+00
    Export_15  energybalance_15   1.000000000000e+00
    Export_15  min_Export_15   1.000000000000e+00
    Export_15  maximize_self_consumption  -1.000000000000e+00
    Export_16  energybalance_16   1.000000000000e+00
    Export_16  min_Export_16   1.000000000000e+00
    Export_16  maximize_self_consumption  -1.000000000000e+00
    Export_17  energybalance_17   1.000000000000e+00
    Export_17  min_Export_17   1.000000000000e+00
    Export_17  maximize_self_consumption  -1.000000000000e+00
    Export_18  energybalance_18   1.000000000000e+00
    Export_18  min_Export_18   1.000000000000e+00
    Export_18  maximize_self_consumption  -1.000000000000e+00
    Export_19  energybalance_19   1.000000000000e+00
    Export_19  min_Export_19   1.000000000000e+00
    Export_19  maximize_self_consumption  -1.000000000000e+00
    Export_2  energybalance_2   1.000000000000e+00
    Export_2  min_Export_2   1.000000000000e+00
    Export_2  maximize_self_consumption  -1.000000000000e+00
    Export_20  energybalance_20   1.000000000000e+00
    Export_20  min_Export_20   1.000000000000e+00
    Export_20  maximize_self_consumption  -1.000000000000e+00
    Export_21  energybalance_21   1.000000000000e+00
    Export_21  min_Export_21   1.000000000000e+00
    Export_21  maximize_self_consumption  -1.000000000000e+00
    Export_22  energybalance_22   1.000000000000e+00
    Export_22  min_Export_22   1.000000000000e+00
    Export_22  maximize_self_consumption  -1.000000000000e+00
    Export_23  energybalance_23   1.000000000000e+00
    Export_23  min_Export_23   1.000000000000e+00
    Export_23  maximize_self_consumption  -1.000000000000e+00
    Export_3  energybalance_3   1.000000000000e+00
    Export_3  min_Export_3   1.000000000000e+00
    Export_3  maximize_self_consumption  -1.000000000000e+00
    Export_4  energybalance_4   1.000000000000e+00
    Export_4  min_Export_4   1.000000000000e+00
    Export_4  maximize_self_consumption  -1.000000000000e+00
    Export_5  energybalance_5   1.000000000000e+00
    Export_5  min_Export_5   1.000000000000e+00
    Export_5  maximize_self_consumption  -1.000000000000e+00
    Export_6  energybalance_6   1.000000000000e+00
    Export_6  min_Export_6   1.000000000000e+00
    Export_6  maximize_self_consumption  -1.000000000000e+00
    Export_7  energybalance_7   1.000000000000e+00
    Export_7  min_Export_7   1.000000000000e+00
    Export_7  maximize_self_consumption  -1.000000000000e+00
    Export_8  energybalance_8   1.000000000000e+00
    Export_8  min_Export_8   1.000000000000e+00
    Export_8  maximize_self_consumption  -1.000000000000e+00
    Export_9  energybalance_9   1.000000000000e+00
    Export_9  min_Export_9   1.000000000000e+00
    Export_9  maximize_self_consumption  -1.000000000000e+00
    Import_0  energybalance_0   1.000000000000e+00
    Import_0  min_Import_0   1.000000000000e+00
    Import_0  min_Imports_0   1.000000000000e+00
    Import_0  maximize_self_consumption   1.000000000000e+00
    Import_1  energybalance_1   1.000000000000e+00
    Import_1  min_Import_1   1.000000000000e+00
    Import_1  min_Imports_1   1.000000000000e+00
    Import_1  maximize_self_consumption   1.000000000000e+00
    Import_10  energybalance_10   1.000000000000e+00
    Import_10  min_Import_10   1.000000000000e+00
    Import_10  min_Imports_10   1.000000000000e+00
    Import_10  maximize_self_consumption   1.000000000000e+00
    Import_11  energybalance_11   1.000000000000e+00
    Import_11  min_Import_11   1.000000000000e+00
    Import_11  min_Imports_11   1.000000000000e+00
    Import_11  maximize_self_consumption   1.000000000000e+00
    Import_12  energybalance_12   1.000000000000e+00
    Import_12  min_Import_12   1.000000000000e+00
    Import_12  min_Imports_12   1.000000000000e+00
    Import_12  maximize_self_consumption   1.000000000000e+00
    Import_13  energybalance_13   1.000000000000e+00
    Import_13  min_Import_13   1.000000000000e+00
    Import_13  min_Imports_13   1.000000000000e+00
    Import_13  maximize_self_consumption   1.000000000000e+00
    Import_14  energybalance_14   1.000000000000e+00
    Import_14  min_Import_14   1.000000000000e+00
    Import_14  min_Imports_14   1.000000000000e+00
    Import_14  maximize_self_consumption   1.000000000000e+00
    Import_15  energybalance_15   1.000000000000e+00
    Import_15  min_Import_15   1.000000000000e+00
    Import_15  min_Imports_15   1.000000000000e+00
    Import_15  maximize_self_consumption   1.000000000000e+00
    Import_16  energybalance_16   1.000000000000e+00
    Import_16  min_Import_16   1.000000000000e+00
    Import_16  min_Imports_16   1.000000000000e+00
    Import_16  maximize_self_consumption   1.000000000000e+00
    Import_17  energybalance_17   1.000000000000e+00
    Import_17  min_Import_17   1.000000000000e+00
    Import_17  min_Imports_17   1.000000000000e+00
    Import_17  maximize_self_consumption   1.000000000000e+00
    Import_18  energybalance_18   1.000000000000e+00
    Import_18  min_Import_18   1.000000000000e+00
    Import_18  min_Imports_18   1.000000000000e+00
    Import_18  maximize_self_consumption   1.000000000000e+00
    Import_19  energybalance_19   1.000000000000e+00
    Import_19  min_Import_19   1.000000000000e+00
    Import_19  min_Imports_19   1.000000000000e+00
    Import_19  maximize_self_consumption   1.000000000000e+00
    Import_2  energybalance_2   1.000000000000e+00
    Import_2  min_Import_2   1.000000000000e+00
    Import_2  min_Imports_2   1.000000000000e+00
    Import_2  maximize_self_consumption   1.000000000000e+00
    Import_20  energybalance_20   1.000000000000e+00
    Import_20  min_Import_20   1.000000000000e+00
    Import_20  min_Imports_20   1.000000000000e+00
    Import_20  maximize_self_consumption   1.000000000000e+00
    Import_21  energybalance_21   1.000000000000e+00
    Import_21  min_Import_21   1.000000000000e+00
    Import_21  min_Imports_21   1.000000000000e+00
    Import_21  maximize_self_consumption   1.000000000000e+00
    Import_22  energybalance_22   1.000000000000e+00
    Import_22  min_Import_22   1.000000000000e+00
    Import_22  min_Imports_22   1.000000000000e+00
    Import_22  maximize_self_consumption   1.000000000000e+00
    Import_23  energybalance_23   1.000000000000e+00
    Import_23  min_Import_23   1.000000000000e+00
    Import_23  min_Imports_23   1.000000000000e+00
    Import_23  maximize_self_consumption   1.000000000000e+00
    Import_3  energybalance_3   1.000000000000e+00
    Import_3  min_Import_3   1.000000000000e+00
    Import_3  min_Imports_3   1.000000000000e+00
    Import_3  maximize_self_consumption   1.000000000000e+00
    Import_4  energybalance_4   1.000000000000e+00
    Import_4  min_Import_4   1.000000000000e+00
    Import_4  min_Imports_4   1.000000000000e+00
    Import_4  maximize_self_consumption   1.000000000000e+00
    Import_5  energybalance_5   1.000000000000e+00
    Import_5  min_Import_5   1.000000000000e+00
    Import_5  min_Imports_5   1.000000000000e+00
    Import_5  maximize_self_consumption   1.000000000000e+00
    Import_6  energybalance_6   1.000000000000e+00
    Import_6  min_Import_6   1.000000000000e+00
    Import_6  min_Imports_6   1.000000000000e+00
    Import_6  maximize_self_consumption   1.000000000000e+00
    Import_7  energybalance_7   1.000000000000e+00
    Import_7  min_Import_7   1.000000000000e+00
    Import_7  min_Imports_7   1.000000000000e+00
    Import_7  maximize_self_consumption   1.000000000000e+00
    Import_8  energybalance_8   1.000000000000e+00
    Import_8  min_Import_8   1.000000000000e+00
    Import_8  min_Imports_8   1.000000000000e+00
    Import_8  maximize_self_consumption   1.000000000000e+00
    Import_9  energybalance_9   1.000000000000e+00
    Import_9  min_Import_9   1.000000000000e+00
    Import_9  min_Imports_9   1.000000000000e+00
    Import_9  maximize_self_consumption   1.000000000000e+00
    batt_c_0  energybalance_0   1.000000000000e+00
    batt_c_0  Battery_max_charging_0   1.000000000000e+00
    batt_c_0  Import_constraint0   1.000000000000e+00
    batt_c_0  _Battery_power_calc_0   1.000000000000e+00
    batt_c_0  battery_energy_constraint_0  -9.500000000000e-01
    batt_c_1  energybalance_1   1.000000000000e+00
    batt_c_1  Battery_max_charging_1   1.000000000000e+00
    batt_c_1  Import_constraint1   1.000000000000e+00
    batt_c_1  _Battery_power_calc_1   1.000000000000e+00
    batt_c_1  battery_energy_constraint_1  -9.500000000000e-01
    batt_c_10  energybalance_10   1.000000000000e+00
    batt_c_10  Battery_max_charging_10   1.000000000000e+00
    batt_c_10  Import_constraint10   1.000000000000e+00
    batt_c_10  _Battery_power_calc_10   1.000000000000e+00
    batt_c_10  battery_energy_constraint_10  -9.500000000000e-01
    batt_c_11  energybalance_11   1.000000000000e+00
    batt_c_11  Battery_max_charging_11   1.000000000000e+00
    batt_c_11  Import_constraint11   1.000000000000e+00
    batt_c_11  _Battery_power_calc_11   1.000000000000e+00
    batt_c_11  battery_energy_constraint_11  -9.500000000000e-01
    batt_c_12  energybalance_12   1.000000000000e+00
    batt_c_12  Battery_max_charging_12   1.000000000000e+00
    batt_c_12  Import_constraint12   1.000000000000e+00
    batt_c_12  _Battery_power_calc_12   1.000000000000e+00
    batt_c_12  battery_energy_constraint_12  -9.500000000000e-01
    batt_c_13  energybalance_13   1.000000000000e+00
    batt_c_13  Battery_max_charging_13   1.000000000000e+00
    batt_c_13  Import_constraint13   1.000000000000e+00
    batt_c_13  _Battery_power_calc_13   1.000000000000e+00
    batt_c_13  battery_energy_constraint_13  -9.500000000000e-01
    batt_c_14  energybalance_14   1.000000000000e+00
    batt_c_14  Battery_max_charging_14   1.000000000000e+00
    batt_c_14  Import_constraint14   1.000000000000e+00
    batt_c_14  _Battery_power_calc_14   1.000000000000e+00
    batt_c_14  battery_energy_constraint_14  -9.500000000000e-01
    batt_c_15  energybalance_15   1.000000000000e+00
    batt_c_15  Battery_max_charging_15   1.000000000000e+00
    batt_c_15  Import_constraint15   1.000000000000e+00
    batt_c_15  _Battery_power_calc_15   1.000000000000e+00
    batt_c_15  battery_energy_constraint_15  -9.500000000000e-01
    batt_c_16  energybalance_16   1.000000000000e+00
    batt_c_16  Battery_max_charging_16   1.000000000000e+00
    batt_c_16  Import_constraint16   1.000000000000e+00
    batt_c_16  _Battery_power_calc_16   1.000000000000e+00
    batt_c_16  battery_energy_constraint_16  -9.500000000000e-01
    batt_c_17  energybalance_17   1.000000000000e+00
    batt_c_17  Battery_max_charging_17   1.000000000000e+00
    batt_c_17  Import_constraint17   1.000000000000e+00
    batt_c_17  _Battery_power_calc_17   1.000000000000e+00
    batt_c_17  battery_energy_constraint_17  -9.500000000000e-01
    batt_c_18  energybalance_18   1.000000000000e+00
    batt_c_18  Battery_max_charging_18   1.000000000000e+00
    batt_c_18  Import_constraint18   1.000000000000e+00
    batt_c_18  _Battery_power_calc_18   1.000000000000e+00
    batt_c_18  battery_energy_constraint_18  -9.500000000000e-01
    batt_c_19  energybalance_19   1.000000000000e+00
    batt_c_19  Battery_max_charging_19   1.000000000000e+00
    batt_c_19  Import_constraint19   1.000000000000e+00
    batt_c_19  _Battery_power_calc_19   1.000000000000e+00
    batt_c_19  battery_energy_constraint_19  -9.500000000000e-01
    batt_c_2  energybalance_2   1.000000000000e+00
    batt_c_2  Battery_max_charging_2   1.000000000000e+00
    batt_c_2  Import_constraint2   1.000000000000e+00
    batt_c_2  _Battery_power_calc_2   1.000000000000e+00
    batt_c_2  battery_energy_constraint_2  -9.500000000000e-01
    batt_c_20  energybalance_20   1.000000000000e+00
    batt_c_20  Battery_max_charging_20   1.000000000000e+00
    batt_c_20  Import_constraint20   1.000000000000e+00
    batt_c_20  _Battery_power_calc_20   1.000000000000e+00
    batt_c_20  battery_energy_constraint_20  -9.500000000000e-01
    batt_c_21  energybalance_21   1.000000000000e+00
    batt_c_21  Battery_max_charging_21   1.000000000000e+00
    batt_c_21  Import_constraint21   1.000000000000e+00
    batt_c_21  _Battery_power_calc_21   1.000000000000e+00
    batt_c_21  battery_energy_constraint_21  -9.500000000000e-01
    batt_c_22  energybalance_22   1.000000000000e+00
    batt_c_22  Battery_max_charging_22   1.000000000000e+00
    batt_c_22  Import_constraint22   1.000000000000e+00
    batt_c_22  _Battery_power_calc_22   1.000000000000e+00
    batt_c_22  battery_energy_constraint_22  -9.500000000000e-01
    batt_c_23  energybalance_23   1.000000000000e+00
    batt_c_23  Battery_max_charging_23   1.000000000000e+00
    batt_c_23  Import_constraint23   1.000000000000e+00
    batt_c_23  _Battery_power_calc_23   1.000000000000e+00
    batt_c_23  final_energy_min  -9.500000000000e-01
    batt_c_23  Final_energy_max  -9.500000000000e-01
    batt_c_3  energybalance_3   1.000000000000e+00
    batt_c_3  Battery_max_charging_3   1.000000000000e+00
    batt_c_3  Import_constraint3   1.000000000000e+00
    batt_c_3  _Battery_power_calc_3   1.000000000000e+00
    batt_c_3  battery_energy_constraint_3  -9.500000000000e-01
    batt_c_4  energybalance_4   1.000000000000e+00
    batt_c_4  Battery_max_charging_4   1.000000000000e+00
    batt_c_4  Import_constraint4   1.000000000000e+00
    batt_c_4  _Battery_power_calc_4   1.000000000000e+00
    batt_c_4  battery_energy_constraint_4  -9.500000000000e-01
    batt_c_5  energybalance_5   1.000000000000e+00
    batt_c_5  Battery_max_charging_5   1.000000000000e+00
    batt_c_5  Import_constraint5   1.000000000000e+00
    batt_c_5  _Battery_power_calc_5   1.000000000000e+00
    batt_c_5  battery_energy_constraint_5  -9.500000000000e-01
    batt_c_6  energybalance_6   1.000000000000e+00
    batt_c_6  Battery_max_charging_6   1.000000000000e+00
    batt_c_6  Import_constraint6   1.000000000000e+00
    batt_c_6  _Battery_power_calc_6   1.000000000000e+00
    batt_c_6  battery_energy_constraint_6  -9.500000000000e-01
    batt_c_7  energybalance_7   1.000000000000e+00
    batt_c_7  Battery_max_charging_7   1.000000000000e+00
    batt_c_7  Import_constraint7   1.000000000000e+00
    batt_c_7  _Battery_power_calc_7   1.000000000000e+00
    batt_c_7  battery_energy_constraint_7  -9.500000000000e-01
    batt_c_8  energybalance_8   1.000000000000e+00
    batt_c_8  Battery_max_charging_8   1.000000000000e+00
    batt_c_8  Import_constraint8   1.000000000000e+00
    batt_c_8  _Battery_power_calc_8   1.000000000000e+00
    batt_c_8  battery_energy_constraint_8  -9.500000000000e-01
    batt_c_9  energybalance_9   1.000000000000e+00
    batt_c_9  Battery_max_charging_9   1.000000000000e+00
    batt_c_9  Import_constraint9   1.000000000000e+00
    batt_c_9  _Battery_power_calc_9   1.000000000000e+00
    batt_c_9  battery_energy_constraint_9  -9.500000000000e-01
    batt_d_0  energybalance_0   1.000000000000e+00
    batt_d_0  Battery_max_discharging_0   1.000000000000e+00
    batt_d_0  Export_constraint_0   1.000000000000e+00
    batt_d_0  _Battery_power_calc_0   1.000000000000e+00
    batt_d_0  battery_energy_constraint_0  -1.052631578947e+00
    batt_d_1  energybalance_1   1.000000000000e+00
    batt_d_1  Battery_max_discharging_1   1.000000000000e+00
    batt_d_1  Export_constraint_1   1.000000000000e+00
    batt_d_1  _Battery_power_calc_1   1.000000000000e+00
    batt_d_1  battery_energy_constraint_1  -1.052631578947e+00
    batt_d_10  energybalance_10   1.000000000000e+00
    batt_d_10  Battery_max_discharging_10   1.000000000000e+00
    batt_d_10  Export_constraint_10   1.000000000000e+00
    batt_d_10  _Battery_power_calc_10   1.000000000000e+00
    batt_d_10  battery_energy_constraint_10  -1.052631578947e+00
    batt_d_11  energybalance_11   1.000000000000e+00
    batt_d_11  Battery_max_discharging_11   1.000000000000e+00
    batt_d_11  Export_constraint_11   1.000000000000e+00
    batt_d_11  _Battery_power_calc_11   1.000000000000e+00
    batt_d_11  battery_energy_constraint_11  -1.052631578947e+00
    batt_d_12  energybalance_12   1.000000000000e+00
    batt_d_12  Battery_max_discharging_12   1.000000000000e+00
    batt_d_12  Export_constraint_12   1.000000000000e+00
    batt_d_12  _Battery_power_calc_12   1.000000000000e+00
    batt_d_12  battery_energy_constraint_12  -1.052631578947e+00
    batt_d_13  energybalance_13   1.000000000000e+00
    batt_d_13  Battery_max_discharging_13   1.000000000000e+00
    batt_d_13  Export_constraint_13   1.000000000000e+00
    batt_d_13  _Battery_power_calc_13   1.000000000000e+00
    batt_d_13  battery_energy_constraint_13  -1.052631578947e+00
    batt_d_14  energybalance_14   1.000000000000e+00
    batt_d_14  Battery_max_discharging_14   1.000000000000e+00
    batt_d_14  Export_constraint_14   1.000000000000e+00
    batt_d_14  _Battery_power_calc_14   1.000000000000e+00
    batt_d_14  battery_energy_constraint_14  -1.052631578947e+00
    batt_d_15  energybalance_15   1.000000000000e+00
    batt_d_15  Battery_max_discharging_15   1.000000000000e+00
    batt_d_15  Export_constraint_15   1.000000000000e+00
    batt_d_15  _Battery_power_calc_15   1.000000000000e+00
    batt_d_15  battery_energy_constraint_15  -1.052631578947e+00
    batt_d_16  energybalance_16   1.000000000000e+00
    batt_d_16  Battery_max_discharging_16   1.000000000000e+00
    batt_d_16  Export_constraint_16   1.000000000000e+00
    batt_d_16  _Battery_power_calc_16   1.000000000000e+00
    batt_d_16  battery_energy_constraint_16  -1.052631578947e+00
    batt_d_17  energybalance_17   1.000000000000e+00
    batt_d_17  Battery_max_discharging_17   1.000000000000e+00
    batt_d_17  Export_constraint_17   1.000000000000e+00
    batt_d_17  _Battery_power_calc_17   1.000000000000e+00
    batt_d_17  battery_energy_constraint_17  -1.052631578947e+00
    batt_d_18  energybalance_18   1.000000000000e+00
    batt_d_18  Battery_max_discharging_18   1.000000000000e+00
    batt_d_18  Export_constraint_18   1.000000000000e+00
    batt_d_18  _Battery_power_calc_18   1.000000000000e+00
    batt_d_18  battery_energy_constraint_18  -1.052631578947e+00
    batt_d_19  energybalance_19   1.000000000000e+00
    batt_d_19  Battery_max_discharging_19   1.000000000000e+00
    batt_d_19  Export_constraint_19   1.000000000000e+00
    batt_d_19  _Battery_power_calc_19   1.000000000000e+00
    batt_d_19  battery_energy_constraint_19  -1.052631578947e+00
    batt_d_2  energybalance_2   1.000000000000e+00
    batt_d_2  Battery_max_discharging_2   1.000000000000e+00
    batt_d_2  Export_constraint_2   1.000000000000e+00
    batt_d_2  _Battery_power_calc_2   1.000000000000e+00
    batt_d_2  battery_energy_constraint_2  -1.052631578947e+00
    batt_d_20  energybalance_20   1.000000000000e+00
    batt_d_20  Battery_max_discharging_20   1.000000000000e+00
    batt_d_20  Export_constraint_20   1.000000000000e+00
    batt_d_20  _Battery_power_calc_20   1.000000000000e+00
    batt_d_20  battery_energy_constraint_20  -1.052631578947e+00
    batt_d_21  energybalance_21   1.000000000000e+00
    batt_d_21  Battery_max_discharging_21   1.000000000000e+00
    batt_d_21  Export_constraint_21   1.000000000000e+00
    batt_d_21  _Battery_power_calc_21   1.000000000000e+00
    batt_d_21  battery_energy_constraint_21  -1.052631578947e+00
    batt_d_22  energybalance_22   1.000000000000e+00
    batt_d_22  Battery_max_discharging_22   1.000000000000e+00
    batt_d_22  Export_constraint_22   1.000000000000e+00
    batt_d_22  _Battery_power_calc_22   1.000000000000e+00
    batt_d_22  battery_energy_constraint_22  -1.052631578947e+00
    batt_d_23  energybalance_23   1.000000000000e+00
    batt_d_23  Battery_max_discharging_23   1.000000000000e+00
    batt_d_23  Export_constraint_23   1.000000000000e+00
    batt_d_23  _Battery_power_calc_23   1.000000000000e+00
    batt_d_23  final_energy_min  -1.052631578947e+00
    batt_d_23  Final_energy_max  -1.052631578947e+00
    batt_d_3  energybalance_3   1.000000000000e+00
    batt_d_3  Battery_max_discharging_3   1.000000000000e+00
    batt_d_3  Export_constraint_3   1.000000000000e+00
    batt_d_3  _Battery_power_calc_3   1.000000000000e+00
    batt_d_3  battery_energy_constraint_3  -1.052631578947e+00
    batt_d_4  energybalance_4   1.000000000000e+00
    batt_d_4  Battery_max_discharging_4   1.000000000000e+00
    batt_d_4  Export_constraint_4   1.000000000000e+00
    batt_d_4  _Battery_power_calc_4   1.000000000000e+00
    batt_d_4  battery_energy_constraint_4  -1.052631578947e+00
    batt_d_5  energybalance_5   1.000000000000e+00
    batt_d_5  Battery_max_discharging_5   1.000000000000e+00
    batt_d_5  Export_constraint_5   1.000000000000e+00
    batt_d_5  _Battery_power_calc_5   1.000000000000e+00
    batt_d_5  battery_energy_constraint_5  -1.052631578947e+00
    batt_d_6  energybalance_6   1.000000000000e+00
    batt_d_6  Battery_max_discharging_6   1.000000000000e+00
    batt_d_6  Export_constraint_6   1.000000000000e+00
    batt_d_6  _Battery_power_calc_6   1.000000000000e+00
    batt_d_6  battery_energy_constraint_6  -1.052631578947e+00
    batt_d_7  energybalance_7   1.000000000000e+00
    batt_d_7  Battery_max_discharging_7   1.000000000000e+00
    batt_d_7  Export_constraint_7   1.000000000000e+00
    batt_d_7  _Battery_power_calc_7   1.000000000000e+00
    batt_d_7  battery_energy_constraint_7  -1.052631578947e+00
    batt_d_8  energybalance_8   1.000000000000e+00
    batt_d_8  Battery_max_discharging_8   1.000000000000e+00
    batt_d_8  Export_constraint_8   1.000000000000e+00
    batt_d_8  _Battery_power_calc_8   1.000000000000e+00
    batt_d_8  battery_energy_constraint_8  -1.052631578947e+00
    batt_d_9  energybalance_9   1.000000000000e+00
    batt_d_9  Battery_max_discharging_9   1.000000000000e+00
    batt_d_9  Export_constraint_9   1.000000000000e+00
    batt_d_9  _Battery_power_calc_9   1.000000000000e+00
    batt_d_9  battery_energy_constraint_9  -1.052631578947e+00
RHS
    RHS       battery_calc_energy_0   1.000000000000e+01
    RHS       energybalance_0   1.700000000000e+01
    RHS       Battery_max_charging_0   0.000000000000e+00
    RHS       Import_constraint0   0.000000000000e+00
    RHS       Battery_max_discharging_0   4.000000000000e+01
    RHS       Export_constraint_0   1.700000000000e+01
    RHS       _Battery_power_calc_0   0.000000000000e+00
    RHS       min_Import_0   0.000000000000e+00
    RHS       min_Export_0   0.000000000000e+00
    RHS       Battery_max_SOC_0   5.000000000000e+01
    RHS       Battery_min_SOC_0   1.000000000000e+01
    RHS       min_Imports_0   1.700000000000e+01
    RHS       energybalance_1   2.000000000000e+00
    RHS       Battery_max_charging_1   0.000000000000e+00
    RHS       Import_constraint1   0.000000000000e+00
    RHS       Battery_max_discharging_1   4.000000000000e+01
    RHS       Export_constraint_1   2.000000000000e+00
    RHS       _Battery_power_calc_1   0.000000000000e+00
    RHS       min_Import_1   0.000000000000e+00
    RHS       min_Export_1   0.000000000000e+00
    RHS       Battery_max_SOC_1   5.000000000000e+01
    RHS       Battery_min_SOC_1   1.000000000000e+01
    RHS       min_Imports_1   2.000000000000e+00
    RHS       energybalance_2   2.000000000000e+00
    RHS       Battery_max_charging_2   0.000000000000e+00
    RHS       Import_constraint2   0.000000000000e+00
    RHS       Battery_max_discharging_2   4.000000000000e+01
    RHS       Export_constraint_2   2.000000000000e+00
    RHS       _Battery_power_calc_2   0.000000000000e+00
    RHS       min_Import_2   0.000000000000e+00
    RHS       min_Export_2   0.000000000000e+00
    RHS       Battery_max_SOC_2   5.000000000000e+01
    RHS       Battery_min_SOC_2   1.000000000000e+01
    RHS       min_Imports_2   2.000000000000e+00
    RHS       energybalance_3   2.000000000000e+00
    RHS       Battery_max_charging_3   0.000000000000e+00
    RHS       Import_constraint3   0.000000000000e+00
    RHS       Battery_max_discharging_3   4.000000000000e+01
    RHS       Export_constraint_3   2.000000000000e+00
    RHS       _Battery_power_calc_3   0.000000000000e+00
    RHS       min_Import_3   0.000000000000e+00
    RHS       min_Export_3   0.000000000000e+00
    RHS       Battery_max_SOC_3   5.000000000000e+01
    RHS       Battery_min_SOC_3   1.000000000000e+01
    RHS       min_Imports_3   2.000000000000e+00
    RHS       energybalance_4   1.000000000000e+00
    RHS       Battery_max_charging_4   0.000000000000e+00
    RHS       Import_constraint4   0.000000000000e+00
    RHS       Battery_max_discharging_4   4.000000000000e+01
    RHS       Export_constraint_4   1.000000000000e+00
    RHS       _Battery_power_calc_4   0.000000000000e+00
    RHS       min_Import_4   0.000000000000e+00
    RHS       min_Export_4   0.000000000000e+00
    RHS       Battery_max_SOC_4   5.000000000000e+01
    RHS       Battery_min_SOC_4   1.000000000000e+01
    RHS       min_Imports_4   1.000000000000e+00
    RHS       energybalance_5   1.000000000000e+00
    RHS       Battery_max_charging_5   0.000000000000e+00
    RHS       Import_constraint5   0.000000000000e+00
    RHS       Battery_max_discharging_5   4.000000000000e+01
    RHS       Export_constraint_5   1.000000000000e+00
    RHS       _Battery_power_calc_5   0.000000000000e+00
    RHS       min_Import_5   0.000000000000e+00
    RHS       min_Export_5   0.000000000000e+00
    RHS       Battery_max_SOC_5   5.000000000000e+01
    RHS       Battery_min_SOC_5   1.000000000000e+01
    RHS       min_Imports_5   1.000000000000e+00
    RHS       energybalance_6   3.000000000000e+00
    RHS       Battery_max_charging_6   0.000000000000e+00
    RHS       Import_constraint6   0.000000000000e+00
    RHS       Battery_max_discharging_6   4.000000000000e+01
    RHS       Export_constraint_6   3.000000000000e+00
    RHS       _Battery_power_calc_6   0.000000000000e+00
    RHS       min_Import_6   0.000000000000e+00
    RHS       min_Export_6   0.000000000000e+00
    RHS       Battery_max_SOC_6   5.000000000000e+01
    RHS       Battery_min_SOC_6   1.000000000000e+01
    RHS       min_Imports_6   3.000000000000e+00
    RHS       energybalance_7   3.000000000000e+00
    RHS       Battery_max_charging_7   0.000000000000e+00
    RHS       Import_constraint7   0.000000000000e+00
    RHS       Battery_max_discharging_7   4.000000000000e+01
    RHS       Export_constraint_7   3.000000000000e+00
    RHS       _Battery_power_calc_7   0.000000000000e+00
    RHS       min_Import_7   0.000000000000e+00
    RHS       min_Export_7   0.000000000000e+00
    RHS       Battery_max_SOC_7   5.000000000000e+01
    RHS       Battery_min_SOC_7   1.000000000000e+01
    RHS       min_Imports_7   3.000000000000e+00
    RHS       energybalance_8   4.000000000000e+00
    RHS       Battery_max_charging_8   0.000000000000e+00
    RHS       Import_constraint8  -1.000000000000e+00
    RHS       Battery_max_discharging_8   4.000000000000e+01
    RHS       Export_constraint_8   5.000000000000e+00
    RHS       _Battery_power_calc_8   0.000000000000e+00
    RHS       min_Import_8   0.000000000000e+00
    RHS       min_Export_8   0.000000000000e+00
    RHS       Battery_max_SOC_8   5.000000000000e+01
    RHS       Battery_min_SOC_8   1.000000000000e+01
    RHS       min_Imports_8   5.000000000000e+00
    RHS       energybalance_9   1.200000000000e+01
    RHS       Battery_max_charging_9   0.000000000000e+00
    RHS       Import_constraint9   0.000000000000e+00
    RHS       Battery_max_discharging_9   4.000000000000e+01
    RHS       Export_constraint_9   1.200000000000e+01
    RHS       _Battery_power_calc_9   0.000000000000e+00
    RHS       min_Import_9   0.000000000000e+00
    RHS       min_Export_9   0.000000000000e+00
    RHS       Battery_max_SOC_9   5.000000000000e+01
    RHS       Battery_min_SOC_9   1.000000000000e+01
    RHS       min_Imports_9   1.200000000000e+01
    RHS       energybalance_10   2.000000000000e+00
    RHS       Battery_max_charging_10   0.000000000000e+00
    RHS       Import_constraint10  -2.000000000000e+00
    RHS       Battery_max_discharging_10   4.000000000000e+01
    RHS       Export_constraint_10   4.000000000000e+00
    RHS       _Battery_power_calc_10   0.000000000000e+00
    RHS       min_Import_10   0.000000000000e+00
    RHS       min_Export_10   0.000000000000e+00
    RHS       Battery_max_SOC_10   5.000000000000e+01
    RHS       Battery_min_SOC_10   1.000000000000e+01
    RHS       min_Imports_10   4.000000000000e+00
    RHS       energybalance_11   2.000000000000e+00
    RHS       Battery_max_charging_11   0.000000000000e+00
    RHS       Import_constraint11  -3.000000000000e+00
    RHS       Battery_max_discharging_11   4.000000000000e+01
    RHS       Export_constraint_11   5.000000000000e+00
    RHS       _Battery_power_calc_11   0.000000000000e+00
    RHS       min_Import_11   0.000000000000e+00
    RHS       min_Export_11   0.000000000000e+00
    RHS       Battery_max_SOC_11   5.000000000000e+01
    RHS       Battery_min_SOC_11   1.000000000000e+01
    RHS       min_Imports_11   5.000000000000e+00
    RHS       energybalance_12  -3.000000000000e+00
    RHS       Battery_max_charging_12   0.000000000000e+00
    RHS       Import_constraint12  -6.000000000000e+00
    RHS       Battery_max_discharging_12   4.000000000000e+01
    RHS       Export_constraint_12   3.000000000000e+00
    RHS       _Battery_power_calc_12   0.000000000000e+00
    RHS       min_Import_12   0.000000000000e+00
    RHS       min_Export_12   0.000000000000e+00
    RHS       Battery_max_SOC_12   5.000000000000e+01
    RHS       Battery_min_SOC_12   1.000000000000e+01
    RHS       min_Imports_12   3.000000000000e+00
    RHS       min_Exports_12   3.000000000000e+00
    RHS       energybalance_13   3.800000000000e+01
    RHS       Battery_max_charging_13   0.000000000000e+00
    RHS       Import_constraint13  -2.000000000000e+00
    RHS       Battery_max_discharging_13   4.000000000000e+01
    RHS       Export_constraint_13   4.000000000000e+01
    RHS       _Battery_power_calc_13   0.000000000000e+00
    RHS       min_Import_13   0.000000000000e+00
    RHS       min_Export_13   0.000000000000e+00
    RHS       Battery_max_SOC_13   5.000000000000e+01
    RHS       Battery_min_SOC_13   1.000000000000e+01
    RHS       min_Imports_13   4.000000000000e+01
    RHS       energybalance_14   1.000000000000e+00
    RHS       Battery_max_charging_14   0.000000000000e+00
    RHS       Import_constraint14  -2.000000000000e+00
    RHS       Battery_max_discharging_14   4.000000000000e+01
    RHS       Export_constraint_14   3.000000000000e+00
    RHS       _Battery_power_calc_14   0.000000000000e+00
    RHS       min_Import_14   0.000000000000e+00
    RHS       min_Export_14   0.000000000000e+00
    RHS       Battery_max_SOC_14   5.000000000000e+01
    RHS       Battery_min_SOC_14   1.000000000000e+01
    RHS       min_Imports_14   3.000000000000e+00
    RHS       energybalance_15   3.000000000000e+00
    RHS       Battery_max_charging_15   0.000000000000e+00
    RHS       Import_constraint15  -1.000000000000e+00
    RHS       Battery_max_discharging_15   4.000000000000e+01
    RHS       Export_constraint_15   4.000000000000e+00
    RHS       _Battery_power_calc_15   0.000000000000e+00
    RHS       min_Import_15   0.000000000000e+00
    RHS       min_Export_15   0.000000000000e+00
    RHS       Battery_max_SOC_15   5.000000000000e+01
    RHS       Battery_min_SOC_15   1.000000000000e+01
    RHS       min_Imports_15   4.000000000000e+00
    RHS       energybalance_16   3.000000000000e+00
    RHS       Battery_max_charging_16   0.000000000000e+00
    RHS       Import_constraint16  -1.000000000000e+00
    RHS       Battery_max_discharging_16   4.000000000000e+01
    RHS       Export_constraint_16   4.000000000000e+00
    RHS       _Battery_power_calc_16   0.000000000000e+00
    RHS       min_Import_16   0.000000000000e+00
    RHS       min_Export_16   0.000000000000e+00
    RHS       Battery_max_SOC_16   5.000000000000e+01
    RHS       Battery_min_SOC_16   1.000000000000e+01
    RHS       min_Imports_16   4.000000000000e+00
    RHS       energybalance_17   4.000000000000e+00
    RHS       Battery_max_charging_17   0.000000000000e+00
    RHS       Import_constraint17   0.000000000000e+00
    RHS       Battery_max_discharging_17   4.000000000000e+01
    RHS       Export_constraint_17   4.000000000000e+00
    RHS       _Battery_power_calc_17   0.000000000000e+00
    RHS       min_Import_17   0.000000000000e+00
    RHS       min_Export_17   0.000000000000e+00
    RHS       Battery_max_SOC_17   5.000000000000e+01
    RHS       Battery_min_SOC_17   1.000000000000e+01
    RHS       min_Imports_17   4.000000000000e+00
    RHS       energybalance_18   2.000000000000e+00
    RHS       Battery_max_charging_18   0.000000000000e+00
    RHS       Import_constraint18   0.000000000000e+00
    RHS       Battery_max_discharging_18   4.000000000000e+01
    RHS       Export_constraint_18   2.000000000000e+00
    RHS       _Battery_power_calc_18   0.000000000000e+00
    RHS       min_Import_18   0.000000000000e+00
    RHS       min_Export_18   0.000000000000e+00
    RHS       Battery_max_SOC_18   5.000000000000e+01
    RHS       Battery_min_SOC_18   1.000000000000e+01
    RHS       min_Imports_18   2.000000000000e+00
    RHS       energybalance_19   2.000000000000e+00
    RHS       Battery_max_charging_19   0.000000000000e+00
    RHS       Import_constraint19   0.000000000000e+00
    RHS       Battery_max_discharging_19   4.000000000000e+01
    RHS       Export_constraint_19   2.000000000000e+00
    RHS       _Battery_power_calc_19   0.000000000000e+00
    RHS       min_Import_19   0.000000000000e+00
    RHS       min_Export_19   0.000000000000e+00
    RHS       Battery_max_SOC_19   5.000000000000e+01
    RHS       Battery_min_SOC_19   1.000000000000e+01
    RHS       min_Imports_19   2.000000000000e+00
    RHS       energybalance_20   2.000000000000e+00
    RHS       Battery_max_charging_20   0.000000000000e+00
    RHS       Import_constraint20   0.000000000000e+00
    RHS       Battery_max_discharging_20   4.000000000000e+01
    RHS       Export_constraint_20   2.000000000000e+00
    RHS       _Battery_power_calc_20   0.000000000000e+00
    RHS       min_Import_20   0.000000000000e+00
    RHS       min_Export_20   0.000000000000e+00
    RHS       Battery_max_SOC_20   5.000000000000e+01
    RHS       Battery_min_SOC_20   1.000000000000e+01
    RHS       min_Imports_20   2.000000000000e+00
    RHS       energybalance_21   1.000000000000e+00
    RHS       Battery_max_charging_21   0.000000000000e+00
    RHS       Import_constraint21   0.000000000000e+00
    RHS       Battery_max_discharging_21   4.000000000000e+01
    RHS       Export_constraint_21   1.000000000000e+00
    RHS       _Battery_power_calc_21   0.000000000000e+00
    RHS       min_Import_21   0.000000000000e+00
    RHS       min_Export_21   0.000000000000e+00
    RHS       Battery_max_SOC_21   5.000000000000e+01
    RHS       Battery_min_SOC_21   1.000000000000e+01
    RHS       min_Imports_21   1.000000000000e+00
    RHS       energybalance_22   1.000000000000e+00
    RHS       Battery_max_charging_22   0.000000000000e+00
    RHS       Import_constraint22   0.000000000000e+00
    RHS       Battery_max_discharging_22   4.000000000000e+01
    RHS       Export_constraint_22   1.000000000000e+00
    RHS       _Battery_power_calc_22   0.000000000000e+00
    RHS       min_Import_22   0.000000000000e+00
    RHS       min_Export_22   0.000000000000e+00
    RHS       Battery_max_SOC_22   5.000000000000e+01
    RHS       Battery_min_SOC_22   1.000000000000e+01
    RHS       min_Imports_22   1.000000000000e+00
    RHS       energybalance_23   1.000000000000e+00
    RHS       Battery_max_charging_23   0.000000000000e+00
    RHS       Import_constraint23   0.000000000000e+00
    RHS       Battery_max_discharging_23   4.000000000000e+01
    RHS       Export_constraint_23   1.000000000000e+00
    RHS       _Battery_power_calc_23   0.000000000000e+00
    RHS       min_Import_23   0.000000000000e+00
    RHS       min_Export_23   0.000000000000e+00
    RHS       Battery_max_SOC_23   5.000000000000e+01
    RHS       Battery_min_SOC_23   1.000000000000e+01
    RHS       min_Imports_23   1.000000000000e+00
    RHS       final_energy_min   1.000000000000e+01
    RHS       Final_energy_max   5.000000000000e+01
    RHS       battery_energy_constraint_0   0.000000000000e+00
    RHS       battery_energy_constraint_1   0.000000000000e+00
    RHS       battery_energy_constraint_2   0.000000000000e+00
    RHS       battery_energy_constraint_3   0.000000000000e+00
    RHS       battery_energy_constraint_4   0.000000000000e+00
    RHS       battery_energy_constraint_5   0.000000000000e+00
    RHS       battery_energy_constraint_6   0.000000000000e+00
    RHS       battery_energy_constraint_7   0.000000000000e+00
    RHS       battery_energy_constraint_8   0.000000000000e+00
    RHS       battery_energy_constraint_9   0.000000000000e+00
    RHS       battery_energy_constraint_10   0.000000000000e+00
    RHS       battery_energy_constraint_11   0.000000000000e+00
    RHS       battery_energy_constraint_12   0.000000000000e+00
    RHS       battery_energy_constraint_13   0.000000000000e+00
    RHS       battery_energy_constraint_14   0.000000000000e+00
    RHS       battery_energy_constraint_15   0.000000000000e+00
    RHS       battery_energy_constraint_16   0.000000000000e+00
    RHS       battery_energy_constraint_17   0.000000000000e+00
    RHS       battery_energy_constraint_18   0.000000000000e+00
    RHS       battery_energy_constraint_19   0.000000000000e+00
    RHS       battery_energy_constraint_20   0.000000000000e+00
    RHS       battery_energy_constraint_21   0.000000000000e+00
    RHS       battery_energy_constraint_22   0.000000000000e+00
BOUNDS
 LO BND       Bat_en_0   1.000000000000e+01
 UP BND       Bat_en_0   5.000000000000e+01
 LO BND       Bat_en_1   1.000000000000e+01
 UP BND       Bat_en_1   5.000000000000e+01
 LO BND       Bat_en_10   1.000000000000e+01
 UP BND       Bat_en_10   5.000000000000e+01
 LO BND       Bat_en_11   1.000000000000e+01
 UP BND       Bat_en_11   5.000000000000e+01
 LO BND       Bat_en_12   1.000000000000e+01
 UP BND       Bat_en_12   5.000000000000e+01
 LO BND       Bat_en_13   1.000000000000e+01
 UP BND       Bat_en_13   5.000000000000e+01
 LO BND       Bat_en_14   1.000000000000e+01
 UP BND       Bat_en_14   5.000000000000e+01
 LO BND       Bat_en_15   1.000000000000e+01
 UP BND       Bat_en_15   5.000000000000e+01
 LO BND       Bat_en_16   1.000000000000e+01
 UP BND       Bat_en_16   5.000000000000e+01
 LO BND       Bat_en_17   1.000000000000e+01
 UP BND       Bat_en_17   5.000000000000e+01
 LO BND       Bat_en_18   1.000000000000e+01
 UP BND       Bat_en_18   5.000000000000e+01
 LO BND       Bat_en_19   1.000000000000e+01
 UP BND       Bat_en_19   5.000000000000e+01
 LO BND       Bat_en_2   1.000000000000e+01
 UP BND       Bat_en_2   5.000000000000e+01
 LO BND       Bat_en_20   1.000000000000e+01
 UP BND       Bat_en_20   5.000000000000e+01
 LO BND       Bat_en_21   1.000000000000e+01
 UP BND       Bat_en_21   5.000000000000e+01
 LO BND       Bat_en_22   1.000000000000e+01
 UP BND       Bat_en_22   5.000000000000e+01
 LO BND       Bat_en_23   1.000000000000e+01
 UP BND       Bat_en_23   5.000000000000e+01
 LO BND       Bat_en_3   1.000000000000e+01
 UP BND       Bat_en_3   5.000000000000e+01
 LO BND       Bat_en_4   1.000000000000e+01
 UP BND       Bat_en_4   5.000000000000e+01
 LO BND       Bat_en_5   1.000000000000e+01
 UP BND       Bat_en_5   5.000000000000e+01
 LO BND       Bat_en_6   1.000000000000e+01
 UP BND       Bat_en_6   5.000000000000e+01
 LO BND       Bat_en_7   1.000000000000e+01
 UP BND       Bat_en_7   5.000000000000e+01
 LO BND       Bat_en_8   1.000000000000e+01
 UP BND       Bat_en_8   5.000000000000e+01
 LO BND       Bat_en_9   1.000000000000e+01
 UP BND       Bat_en_9   5.000000000000e+01
 LO BND       Bat_pow_0  -4.000000000000e+01
 UP BND       Bat_pow_0   4.000000000000e+01
 LO BND       Bat_pow_1  -4.000000000000e+01
 UP BND       Bat_pow_1   4.000000000000e+01
 LO BND       Bat_pow_10  -4.000000000000e+01
 UP BND       Bat_pow_10   4.000000000000e+01
 LO BND       Bat_pow_11  -4.000000000000e+01
 UP BND       Bat_pow_11   4.000000000000e+01
 LO BND       Bat_pow_12  -4.000000000000e+01
 UP BND       Bat_pow_12   4.000000000000e+01
 LO BND       Bat_pow_13  -4.000000000000e+01
 UP BND       Bat_pow_13   4.000000000000e+01
 LO BND       Bat_pow_14  -4.000000000000e+01
 UP BND       Bat_pow_14   4.000000000000e+01
 LO BND       Bat_pow_15  -4.000000000000e+01
 UP BND       Bat_pow_15   4.000000000000e+01
 LO BND       Bat_pow_16  -4.000000000000e+01
 UP BND       Bat_pow_16   4.000000000000e+01
 LO BND       Bat_pow_17  -4.000000000000e+01
 UP BND       Bat_pow_17   4.000000000000e+01
 LO BND       Bat_pow_18  -4.000000000000e+01
 UP BND       Bat_pow_18   4.000000000000e+01
 LO BND       Bat_pow_19  -4.000000000000e+01
 UP BND       Bat_pow_19   4.000000000000e+01
 LO BND       Bat_pow_2  -4.000000000000e+01
 UP BND       Bat_pow_2   4.000000000000e+01
 LO BND       Bat_pow_20  -4.000000000000e+01
 UP BND       Bat_pow_20   4.000000000000e+01
 LO BND       Bat_pow_21  -4.000000000000e+01
 UP BND       Bat_pow_21   4.000000000000e+01
 LO BND       Bat_pow_22  -4.000000000000e+01
 UP BND       Bat_pow_22   4.000000000000e+01
 LO BND       Bat_pow_23  -4.000000000000e+01
 UP BND       Bat_pow_23   4.000000000000e+01
 LO BND       Bat_pow_3  -4.000000000000e+01
 UP BND       Bat_pow_3   4.000000000000e+01
 LO BND       Bat_pow_4  -4.000000000000e+01
 UP BND       Bat_pow_4   4.000000000000e+01
 LO BND       Bat_pow_5  -4.000000000000e+01
 UP BND       Bat_pow_5   4.000000000000e+01
 LO BND       Bat_pow_6  -4.000000000000e+01
 UP BND       Bat_pow_6   4.000000000000e+01
 LO BND       Bat_pow_7  -4.000000000000e+01
 UP BND       Bat_pow_7   4.000000000000e+01
 LO BND       Bat_pow_8  -4.000000000000e+01
 UP BND       Bat_pow_8   4.000000000000e+01
 LO BND       Bat_pow_9  -4.000000000000e+01
 UP BND       Bat_pow_9   4.000000000000e+01
 BV BND       Bat_st_0
 BV BND       Bat_st_1
 BV BND       Bat_st_10
 BV BND       Bat_st_11
 BV BND       Bat_st_12
 BV BND       Bat_st_13
 BV BND       Bat_st_14
 BV BND       Bat_st_15
 BV BND       Bat_st_16
 BV BND       Bat_st_17
 BV BND       Bat_st_18
 BV BND       Bat_st_19
 BV BND       Bat_st_2
 BV BND       Bat_st_20
 BV BND       Bat_st_21
 BV BND       Bat_st_22
 BV BND       Bat_st_23
 BV BND       Bat_st_3
 BV BND       Bat_st_4
 BV BND       Bat_st_5
 BV BND       Bat_st_6
 BV BND       Bat_st_7
 BV BND       Bat_st_8
 BV BND       Bat_st_9
 LO BND       Export_0  -2.000000000000e+01
 UP BND       Export_0   0.000000000000e+00
 LO BND       Export_1  -2.000000000000e+01
 UP BND       Export_1   0.000000000000e+00
 LO BND       Export_10  -2.000000000000e+01
 UP BND       Export_10   0.000000000000e+00
 LO BND       Export_11  -2.000000000000e+01
 UP BND       Export_11   0.000000000000e+00
 LO BND       Export_12  -2.000000000000e+01
 UP BND       Export_12   0.000000000000e+00
 LO BND       Export_13  -2.000000000000e+01
 UP BND       Export_13   0.000000000000e+00
 LO BND       Export_14  -2.000000000000e+01
 UP BND       Export_14   0.000000000000e+00
 LO BND       Export_15  -2.000000000000e+01
 UP BND       Export_15   0.000000000000e+00
 LO BND       Export_16  -2.000000000000e+01
 UP BND       Export_16   0.000000000000e+00
 LO BND       Export_17  -2.000000000000e+01
 UP BND       Export_17   0.000000000000e+00
 LO BND       Export_18  -2.000000000000e+01
 UP BND       Export_18   0.000000000000e+00
 LO BND       Export_19  -2.000000000000e+01
 UP BND       Export_19   0.000000000000e+00
 LO BND       Export_2  -2.000000000000e+01
 UP BND       Export_2   0.000000000000e+00
 LO BND       Export_20  -2.000000000000e+01
 UP BND       Export_20   0.000000000000e+00
 LO BND       Export_21  -2.000000000000e+01
 UP BND       Export_21   0.000000000000e+00
 LO BND       Export_22  -2.000000000000e+01
 UP BND       Export_22   0.000000000000e+00
 LO BND       Export_23  -2.000000000000e+01
 UP BND       Export_23   0.000000000000e+00
 LO BND       Export_3  -2.000000000000e+01
 UP BND       Export_3   0.000000000000e+00
 LO BND       Export_4  -2.000000000000e+01
 UP BND       Export_4   0.000000000000e+00
 LO BND       Export_5  -2.000000000000e+01
 UP BND       Export_5   0.000000000000e+00
 LO BND       Export_6  -2.000000000000e+01
 UP BND       Export_6   0.000000000000e+00
 LO BND       Export_7  -2.000000000000e+01
 UP BND       Export_7   0.000000000000e+00
 LO BND       Export_8  -2.000000000000e+01
 UP BND       Export_8   0.000000000000e+00
 LO BND       Export_9  -2.000000000000e+01
 UP BND       Export_9   0.000000000000e+00
 UP BND       Import_0   2.500000000000e+01
 UP BND       Import_1   2.500000000000e+01
 UP BND       Import_10   2.500000000000e+01
 UP BND       Import_11   2.500000000000e+01
 UP BND       Import_12   2.500000000000e+01
 UP BND       Import_13   2.500000000000e+01
 UP BND       Import_14   2.500000000000e+01
 UP BND       Import_15   2.500000000000e+01
 UP BND       Import_16   2.500000000000e+01
 UP BND       Import_17   2.500000000000e+01
 UP BND       Import_18   2.500000000000e+01
 UP BND       Import_19   2.500000000000e+01
 UP BND       Import_2   2.500000000000e+01
 UP BND       Import_20   2.500000000000e+01
 UP BND       Import_21   2.500000000000e+01
 UP BND       Import_22   2.500000000000e+01
 UP BND       Import_23   2.500000000000e+01
 UP BND       Import_3   2.500000000000e+01
 UP BND       Import_4   2.500000000000e+01
 UP BND       Import_5   2.500000000000e+01
 UP BND       Import_6   2.500000000000e+01
 UP BND       Import_7   2.500000000000e+01
 UP BND       Import_8   2.500000000000e+01
 UP BND       Import_9   2.500000000000e+01
 LO BND       batt_c_0  -4.000000000000e+01
 UP BND       batt_c_0   0.000000000000e+00
 LO BND       batt_c_1  -4.000000000000e+01
 UP BND       batt_c_1   0.000000000000e+00
 LO BND       batt_c_10  -4.000000000000e+01
 UP BND       batt_c_10   0.000000000000e+00
 LO BND       batt_c_11  -4.000000000000e+01
 UP BND       batt_c_11   0.000000000000e+00
 LO BND       batt_c_12  -4.000000000000e+01
 UP BND       batt_c_12   0.000000000000e+00
 LO BND       batt_c_13  -4.000000000000e+01
 UP BND       batt_c_13   0.000000000000e+00
 LO BND       batt_c_14  -4.000000000000e+01
 UP BND       batt_c_14   0.000000000000e+00
 LO BND       batt_c_15  -4.000000000000e+01
 UP BND       batt_c_15   0.000000000000e+00
 LO BND       batt_c_16  -4.000000000000e+01
 UP BND       batt_c_16   0.000000000000e+00
 LO BND       batt_c_17  -4.000000000000e+01
 UP BND       batt_c_17   0.000000000000e+00
 LO BND       batt_c_18  -4.000000000000e+01
 UP BND       batt_c_18   0.000000000000e+00
 LO BND       batt_c_19  -4.000000000000e+01
 UP BND       batt_c_19   0.000000000000e+00
 LO BND       batt_c_2  -4.000000000000e+01
 UP BND       batt_c_2   0.000000000000e+00
 LO BND       batt_c_20  -4.000000000000e+01
 UP BND       batt_c_20   0.000000000000e+00
 LO BND       batt_c_21  -4.000000000000e+01
 UP BND       batt_c_21   0.000000000000e+00
 LO BND       batt_c_22  -4.000000000000e+01
 UP BND       batt_c_22   0.000000000000e+00
 LO BND       batt_c_23  -4.000000000000e+01
 UP BND       batt_c_23   0.000000000000e+00
 LO BND       batt_c_3  -4.000000000000e+01
 UP BND       batt_c_3   0.000000000000e+00
 LO BND       batt_c_4  -4.000000000000e+01
 UP BND       batt_c_4   0.000000000000e+00
 LO BND       batt_c_5  -4.000000000000e+01
 UP BND       batt_c_5   0.000000000000e+00
 LO BND       batt_c_6  -4.000000000000e+01
 UP BND       batt_c_6   0.000000000000e+00
 LO BND       batt_c_7  -4.000000000000e+01
 UP BND       batt_c_7   0.000000000000e+00
 LO BND       batt_c_8  -4.000000000000e+01
 UP BND       batt_c_8   0.000000000000e+00
 LO BND       batt_c_9  -4.000000000000e+01
 UP BND       batt_c_9   0.000000000000e+00
 UP BND       batt_d_0   4.000000000000e+01
 UP BND       batt_d_1   4.000000000000e+01
 UP BND       batt_d_10   4.000000000000e+01
 UP BND       batt_d_11   4.000000000000e+01
 UP BND       batt_d_12   4.000000000000e+01
 UP BND       batt_d_13   4.000000000000e+01
 UP BND       batt_d_14   4.000000000000e+01
 UP BND       batt_d_15   4.000000000000e+01
 UP BND       batt_d_16   4.000000000000e+01
 UP BND       batt_d_17   4.000000000000e+01
 UP BND       batt_d_18   4.000000000000e+01
 UP BND       batt_d_19   4.000000000000e+01
 UP BND       batt_d_2   4.000000000000e+01
 UP BND       batt_d_20   4.000000000000e+01
 UP BND       batt_d_21   4.000000000000e+01
 UP BND       batt_d_22   4.000000000000e+01
 UP BND       batt_d_23   4.000000000000e+01
 UP BND       batt_d_3   4.000000000000e+01
 UP BND       batt_d_4   4.000000000000e+01
 UP BND       batt_d_5   4.000000000000e+01
 UP BND       batt_d_6   4.000000000000e+01
 UP BND       batt_d_7   4.000000000000e+01
 UP BND       batt_d_8   4.000000000000e+01
 UP BND       batt_d_9   4.000000000000e+01
ENDATA
